function doLogin (url){
    var formData = new FormData();
    formData.append("username", $('input[name=username]').val());
    formData.append("password", $('input[name=password]').val());

    $.ajax({
        type: "POST",
        url: url + "/Admin/doLogin",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {



            var response_brought = JSON.parse(response);
            if (response_brought["status"] == "success") {
                swal({
                    type: 'success',
                    title: 'Tebrikler!',
                    text: 'Başarıyla giriş yaptınız...',
                    footer: response_brought["message"]
                })
                setTimeout(function () {
                    location.reload();
                }, 1500);
            } else {

                swal({
                    type: 'error',
                    title: 'Hata...',
                    text: 'Birşeyler yanlış gitti!',
                    footer: response_brought["message"]
                })
            }
        }
    });
}
