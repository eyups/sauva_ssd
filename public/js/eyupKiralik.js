function submit_form(url)
{
	//Variable declaration and assignment
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var fullname = $("#name").val();
	var email = $("#email").val();
	var phone = $("#cepTel").val();
	var adres = $("#adres").val();
	var ilce = $("#ilce").val();
	var il = $("#il").val();
	var magazaAlani = $("#magazaAlani", "#second").val();
	var digerAlan = $("#digerAlan", "#second").val();
	var kiraBedeli = $("#kiraBedeli", "#second").val();
	var message = $("#message", "#second").val();
	var vpb_captcha_code = grecaptcha.getResponse();


	if( fullname == "" ) //Validation against empty field for fullname
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen tam isminizi gerekli alana doldurun. Teşekkürler.</div>');
		$("#name").focus();
	}
	else if( email == "" ) //Validation against empty field for email address
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Kiralık yer önerinizin işleme alınabilmesi için email adresi gereklidir. Teşekkürler.</div>');
		$("#email").focus();
	}
	else if(reg.test(email) == false) //Validation for working email address
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Üzgünüm, email adresiniz yanlış. Lütfen işleme alınabilmesi için geçerli bir email giriniz. Teşekkürler.</div>');
		$("#email").focus();
	}
	else if( phone == "" ) //Validation against empty field for telephone number
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Telefon numaranız size daha iyi geri dönüş yapabilmemiz için gereklidir. Lütfen telefon numaranızı giriniz.</div>');
		$("#cepTel").focus();
	}
	else if( adres == "" ) //Validation against empty field for telephone number
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Mağazanızın açık adresini belirtmediniz.</div>');
		$("#adres").focus();
	}
	else if( ilce == "" ) //Validation against empty field for email subject
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen mağazanın bulunduğu ilçeyi yazınız. Teşekkürler.</div>');
		$("#ilce").focus();
	}
	else if( il == "" ) //Validation against empty field for email subject
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen mağazanın bulunduğu ili yazınız. Teşekkürler.</div>');
		$("#il").focus();
	}
	else if( magazaAlani == "" ) //Validation against empty field for email subject
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen mağazanın toplam alanını yazınız. Teşekkürler.</div>');
		$("#magazaAlani").focus();
	}
	else if( kiraBedeli == "" ) //Validation against empty field for email subject
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen mağazanın kira bedelini yazınız. Teşekkürler.</div>');
		$("#kiraBedeli").focus();
	}
	else if( vpb_captcha_code == "" ) //Validation against empty field for security captcha code
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen Güvenlik sorusunu yanıtlayınız. Teşekkürler.</div>');
		$("#g-recaptcha-response").focus();
	}
	else
	{
		var dataString = 'name=' + fullname + '&email=' + email + '&cepTel=' + phone + '&il=' + il + '&ilce=' + ilce + '&magazaAlani=' + magazaAlani  + '&digerAlan='
		+ digerAlan + '&kiraBedeli=' + kiraBedeli + '&message=' + message + '&adres=' + adres + '&g-recaptcha-response=' + vpb_captcha_code + '&submitted=1';
		$.ajax({
			type: "POST",
			url: url+"/KiralikYer/sendMail",
			data: dataString,
			beforeSend: function()
			{
				//Show loading image
				$("#response_brought").html('<br clear="all"><div class="alert alert-info">Lütfen bekleyin    <img src="'+ url +'/public/images/loading2.gif" alt="Gönderiliyor...." align="absmiddle" title="Gönderiliyor...."/></div>');

			},
			success: function(response)
			{
				//Check to see if the message is sent or not
				var response_brought = response.indexOf('Tebrikler');
				if( response_brought == -1 )
				{
					//Display error message is the message is not sent
					 $("#response_brought").html(response);
					 grecaptcha.reset();
				}
				else
				{
					 //Clear all form fields on success
					$("#name").val('');
					$("#email").val('');
					$("#cepTel").val('');
					$("#adres").val('');
					$("#ilce").val('');
					$("#il").val('');
					$("#magazaAlani").val('');
					$("#digerAlan").val('');
					$("#kiraBedeli").val('');
					$("#message", "#second").val('');
					$("#code").val('');
					grecaptcha.reset();
					//Display success message if the message is sent
					$("#response_brought").html(response);

					//Remove the success message also after a while of displaying the message to the user
					setTimeout(function() {
						$("#response_brought").html('');
					},10000);
				}
			}
		});
	}
}
