var myCenter=new google.maps.LatLng(40.0181234,29.524399);
var sube1 = new google.maps.LatLng(40.0753432,29.5100588);
var sube2 = new google.maps.LatLng(40.0795284,29.5140636);
var sube3 = new google.maps.LatLng(40.0792099,29.509571);
var sube4 = new google.maps.LatLng(40.0813544,29.510008);
var sube5 = new google.maps.LatLng(40.0639987,29.5082679);
var sube6 = new google.maps.LatLng(40.0775439,29.5025928);
var sube9 = new google.maps.LatLng(40.0874003,29.4190257);
var sube10 = new google.maps.LatLng(39.8057385,29.610794);
var sube12 = new google.maps.LatLng(40.0677696,29.5091562);
var sube13 = new google.maps.LatLng(40.1160019,29.5139984);
var sube14 = new google.maps.LatLng(40.0823711,29.5292214);
var sube15 = new google.maps.LatLng(40.0849578,29.5033543);
var sube16 = new google.maps.LatLng(40.0799475,29.4965413);
var sube17 = new google.maps.LatLng(40.2667038,29.6513556);
var sube18 = new google.maps.LatLng(40.1069948,29.5209011);
var sube19 = new google.maps.LatLng(39.7835682,30.5186093);
var sube20 = new google.maps.LatLng(39.7709253,30.5282898);
var sube21 = new google.maps.LatLng(39.799626,30.491258);
var sube22 = new google.maps.LatLng(39.7639535,30.545588);
var sube23 = new google.maps.LatLng(39.7570782,30.5588618);
var sube24 = new google.maps.LatLng(40.079735,29.5176825);
var sube25 = new google.maps.LatLng(40.099280, 29.417958);
var sube26 = new google.maps.LatLng(40.118971, 29.467638);
var sube27 = new google.maps.LatLng(40.1100521,29.517922);
var sube28 = new google.maps.LatLng(39.7529519,30.5075754);
var sube29 = new google.maps.LatLng(39.788314,30.524986);
var sube30 = new google.maps.LatLng(40.2633991,29.6498436);
var sube31 = new google.maps.LatLng(39.7693299,30.541862);
var sube32 = new google.maps.LatLng(39.778897,30.483376);
var sube34 = new google.maps.LatLng(40.188555,29.140562);
var sube35 = new google.maps.LatLng(39.905084,30.044082);
var sube36 = new google.maps.LatLng(40.181948, 29.098375);
var sube37 = new google.maps.LatLng(40.1853321,29.1102873);
var sube38 = new google.maps.LatLng(39.773133, 30.505642);
var sube39 = new google.maps.LatLng(39.906495, 30.041120);
var sube40 = new google.maps.LatLng(39.7531575,30.5018557);
var sube41 = new google.maps.LatLng(39.775574, 30.467440);
var sube42 = new google.maps.LatLng(39.793523, 30.477625);
var sube44 = new google.maps.LatLng(40.200719, 29.067740);
var sube45 = new google.maps.LatLng(39.796291, 30.511797);
var sube46 = new google.maps.LatLng(39.7500479,30.571055);
var sube47 = new google.maps.LatLng(39.768532, 30.517977);

var acKapa = 0;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

var markers = new Array();

var lokasyon;

var getURL = window.location.href;

var editURL = getURL.replace("Subeler", "");

var initialLocation;
var browserSupportFlag =  new Boolean();
var enYakinSube = new google.maps.Marker({
	  position:initialLocation,
	  icon:'lokasyon.png',
	  });
var seciliSube = new google.maps.Marker({
	  position:initialLocation,
	  icon:'lokasyon.png',

	  });

function initialize(position)
{

directionsDisplay = new google.maps.DirectionsRenderer();

var mapProp = {
  center:myCenter,
  zoom:9,
  scrollwheel: false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map = new google.maps.Map(document.getElementById("map"),mapProp);

lokasyon=new google.maps.Marker({
	  position:sube1,
	  icon: editURL + 'public/images/lokasyon.png',
	  draggable: true,
	  title:'Şuan buradasınız: Konumunuzun yanlış olduğunu düşünüyorsanız sürükleyebilirsiniz.'
	  });
lokasyon.setMap(map);	  

 // Try HTML5 geolocation
  // Try HTML5 geolocation
  if(navigator.geolocation) 
  {
    navigator.geolocation.getCurrentPosition(function(position) { 
	var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	lokasyon.position = pos;
	lokasyon.setMap(map);
    }, 
	function() 
	{
      handleNoGeolocation(true);
	  //lokasyon.setMap(map);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);

	//lokasyon.setMap(map);
  }


  function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
      var content = 'Hata: Konumunuz bulunurken hata oluştu. Lütfen sayfayı yenileyin';

    } else {
      var content = 'Hata: Tarayıcınız konum bulmayı desteklemiyor.';
    }

    alert(content);

    lokasyon.position = sube1;

    map.setCenter(lokasyon.position);
  }

markers[1]=new google.maps.Marker({
  position:sube1,
  icon: editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Cuma Mah. Atatürk Bulvarı No:75 İnegöl / BURSA'
  });

markers[1].setMap(map);

markers[2]=new google.maps.Marker({
  position:sube2,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yenice Mah. Ankara Cad. No:58 İnegöl / BURSA'
  });

markers[2].setMap(map);

markers[3]=new google.maps.Marker({
  position:sube3,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Cuma Mah. Belediye Cad. No:1/A İnegöl / BURSA'
  });

markers[3].setMap(map);

markers[4]=new google.maps.Marker({
  position:sube4,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Sinanbey Mah. Ahmet Akyollu Cad. No:20 İnegöl / BURSA'
  });

markers[4].setMap(map);

markers[5]=new google.maps.Marker({
  position:sube5,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Hamidiye Mah. Yeşil Barış Sok. No:1 İnegöl / BURSA'
  });

markers[5].setMap(map);

markers[6]=new google.maps.Marker({
  position:sube6,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>K.Paşa Mh. Mahmut Esatbey Cad.No:139 İnegöl / BURSA'
  });

markers[6].setMap(map);

markers[7]=new google.maps.Marker({
  position:sube9,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Orta Mah. Cumhuriyet Meydanı No: 10/A İnegöl / BURSA'
  });

markers[7].setMap(map);

markers[8]=new google.maps.Marker({
  position:sube10,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Hisar Mah. Cumhuriyet Meydanı No: 7/A Domaniç / KÜTAHYA'
  });

markers[8].setMap(map);

markers[9]=new google.maps.Marker({
  position:sube12,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Hamidiye Mah. Park Cad. No:71/A İnegöl / BURSA'
  });

markers[9].setMap(map);

markers[10]=new google.maps.Marker({
  position:sube13,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Esentepe Mah. Karanfil Cad. No:2 İnegöl / BURSA'
  });

markers[10].setMap(map);

markers[11]=new google.maps.Marker({
  position:sube14,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Mesudiye Mah. İnegöl Cad. No:93 İnegöl / BURSA'
  });

markers[11].setMap(map);

markers[12]=new google.maps.Marker({
  position:sube15,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Süleymaniye Mah. Bursa Cad. No:57 İnegöl / BURSA'
  });

markers[12].setMap(map);

markers[13]=new google.maps.Marker({
  position:sube16,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Burhaniye Mah. Mimar Sinan Cad. No:25 İnegöl / BURSA'
  });

markers[13].setMap(map);

markers[14]=new google.maps.Marker({
  position:sube17,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yenigün Mah. Atatürk Cad. No: 15/A Yenişehir /BURSA'
  });

markers[14].setMap(map);

markers[15]=new google.maps.Marker({
  position:sube18,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yunus Emre Mah. Yunus Emre Cad. No: 34  İnegöl / BURSA'
  });

markers[15].setMap(map);

markers[16]=new google.maps.Marker({
  position:sube19,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Güllük Mah. Sakarya-2 Cad. No: 29 Tepebaşı / ESKİŞEHİR'
  });

markers[16].setMap(map);

markers[17]=new google.maps.Marker({
  position:sube20,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Kurtuluş Mah. Ziyapaşa Cad. No: 22/A  Odunpazarı / ESKİŞEHİR'
  });

markers[17].setMap(map);

markers[18]=new google.maps.Marker({
  position:sube21,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Şirintepe Mah. Alınca Cad. No: 1 / A  Tepebaşı / ESKİŞEHİR'
  });

markers[18].setMap(map);

markers[19]=new google.maps.Marker({
  position:sube22,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Huzur Mah. Ali Rıza Efendi Cad. No: 83/A-B   Odunpazarı / ESKİŞEHİR'
  });

markers[19].setMap(map);

markers[20]=new google.maps.Marker({
  position:sube23,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Emek Mah. Ertaş Cad. No: 28/A-B Odunpazarı / ESKİŞEHİR'
  });

markers[20].setMap(map);

markers[21]=new google.maps.Marker({
  position:sube24,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Mahmudiye Mah. Ankara Cad. No:8/A  İnegöl / BURSA'
  });

markers[21].setMap(map);


markers[22]=new google.maps.Marker({
  position:sube26,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Akhisar Mah. Atatürk Cad. No:3 İnegöl / BURSA'
  });

markers[22].setMap(map);

markers[23]=new google.maps.Marker({
  position:sube27,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yunus Emre Mah. Bağlar Cad. No:22 İnegöl / BURSA'
  });

markers[23].setMap(map);

markers[24]=new google.maps.Marker({
  position:sube28,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yıldıztepe Mah. Halk Cad. No:120/A Odunpazarı / ESKİŞEHİR'
  });

markers[24].setMap(map);

markers[25]=new google.maps.Marker({
  position:sube29,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Zafer Mah. Gaffar Okkan Cad. No:67/A Daire No:21/A Tepebaşı / ESKİŞEHİR'
  });

markers[25].setMap(map);

markers[26]=new google.maps.Marker({
  position:sube30,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Hıdırbali Mah. Hacı Hasan Sok. No:4/A Yenişehir  / BURSA'
  });

markers[26].setMap(map);

markers[27]=new google.maps.Marker({
  position:sube31,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Gökmeydan Mah. Ulus Cad. No:65/B Odunpazarı /ESKİŞEHİR'
  });

markers[27].setMap(map);

markers[28]=new google.maps.Marker({
  position:sube32,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Ertuğrulgazi Mah. Mahmut Şevket Cad. No:31/A Tepebaşı / ESKİŞEHİR'
  });

markers[28].setMap(map);

markers[29]=new google.maps.Marker({
  position:sube32,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Ertuğrulgazi Mah. Mahmut Şevket Cad. No:31/A Tepebaşı / ESKİŞEHİR'
  });

markers[29].setMap(map);

markers[30]=new google.maps.Marker({
  position:sube35,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yeni Mah. İsmet İnönü Cad. No:100 Bozüyük / BİLECİK '
  });

markers[30].setMap(map);

markers[31]=new google.maps.Marker({
  position:sube38,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Kırmızıtoprak Mah. Ercan Sok. No:47/A Odunpazarı / ESKİŞEHİR'
  });

markers[31].setMap(map);

markers[32]=new google.maps.Marker({
  position:sube39,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Yeni Mah. Camii Cad. No:12/A Bozüyük / BİLECİK'
  });

markers[32].setMap(map);

markers[33]=new google.maps.Marker({
  position:sube40,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Büyükdere Mah. Çayırlar Sok. No:4/A Odunpazarı / ESKİŞEHİR'
  });

markers[33].setMap(map);

markers[34]=new google.maps.Marker({
  position:sube41,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Çamlıca Mah. Birlik Cad. No:38  / A   Tepebaşı / ESKİŞEHİR'
  });

markers[34].setMap(map);

markers[35]=new google.maps.Marker({
  position:sube42,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Şirintepe Mah. Bursa Cad. No:47 / A Tepebaşı /ESKİŞEHİR'
  });

markers[35].setMap(map);

markers[36]=new google.maps.Marker({
  position:sube45,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Sütlüce Mah. Eğitimciler Cad. No:42/AB  Tepebaşı / ESKİŞEHİR'
  });

markers[36].setMap(map);

markers[37]=new google.maps.Marker({
  position:sube46,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Emek Mah. Ertaş Cad. No:139 Odunpazarı / Eskişehir '
  });

markers[37].setMap(map);

markers[38]=new google.maps.Marker({
  position:sube47,
  icon:editURL + 'public/images/gulenadam.png',
  adres:'<strong>Şube Adres Bilgisi: </strong>Arefiye Mah. Mustafa Kemal Atatürk Cad. No:48/A    Odunpazarı / Eskişehir '
  });

markers[38].setMap(map);

google.maps.event.addListener(lokasyon, 'dragend', function() {
    //$('.menu').toggle("slide");
	mesafeHesap();
  });


  
$(function(){
		google.maps.event.addListener(markers[1], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[1].adres;
		
	  });google.maps.event.addListener(markers[2], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[2].adres;
		
	  });google.maps.event.addListener(markers[3], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[3].adres;
		
	  });google.maps.event.addListener(markers[4], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[4].adres;
		
	  });google.maps.event.addListener(markers[5], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[5].adres;
		
	  });google.maps.event.addListener(markers[6], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[6].adres;
		
	  });google.maps.event.addListener(markers[7], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[7].adres;
		
	  });google.maps.event.addListener(markers[8], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[8].adres;
		
	  });google.maps.event.addListener(markers[9], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[9].adres;
		
	  });google.maps.event.addListener(markers[10], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[10].adres;
		
	  });google.maps.event.addListener(markers[11], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[11].adres;
		
	  });google.maps.event.addListener(markers[12], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[12].adres;
		
	  });google.maps.event.addListener(markers[13], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[13].adres;
		
	  });google.maps.event.addListener(markers[14], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[14].adres;
		
	  });google.maps.event.addListener(markers[15], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[15].adres;
		
	  });google.maps.event.addListener(markers[16], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[16].adres;
		
	  });google.maps.event.addListener(markers[17], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[17].adres;
		
	  });google.maps.event.addListener(markers[18], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[18].adres;
		
	  });google.maps.event.addListener(markers[19], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[19].adres;
		
	  });google.maps.event.addListener(markers[20], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[20].adres;
		
	  });google.maps.event.addListener(markers[21], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[21].adres;
		
	  });google.maps.event.addListener(markers[22], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[22].adres;
		
	  });google.maps.event.addListener(markers[23], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[23].adres;
		
	  });google.maps.event.addListener(markers[24], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[24].adres;
		
	  });google.maps.event.addListener(markers[25], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[25].adres;
		
	  });google.maps.event.addListener(markers[26], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[26].adres;
		
	  });google.maps.event.addListener(markers[27], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[27].adres;
		
	  });google.maps.event.addListener(markers[28], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[28].adres;
		
	  });google.maps.event.addListener(markers[29], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[29].adres;
		
	  });google.maps.event.addListener(markers[30], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[30].adres;
		
	  });google.maps.event.addListener(markers[31], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[31].adres;
		
	  });google.maps.event.addListener(markers[32], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[32].adres;
		
	  });google.maps.event.addListener(markers[33], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[33].adres;
		
	  });google.maps.event.addListener(markers[34], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[34].adres;
		
	  });google.maps.event.addListener(markers[35], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[35].adres;
		
	  });google.maps.event.addListener(markers[36], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[36].adres;
		
	  });google.maps.event.addListener(markers[37], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[37].adres;
		
	  });google.maps.event.addListener(markers[38], 'click', function() {
		if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		document.getElementById("adres").innerHTML = markers[38].adres;
		
	  });	
  var moveLeft = 0;
    var moveDown = 0;
    $('a.popper').hover(function(e) {
		
        var target = '#' + ($(this).attr('data-popbox'));
         
        $(target).show();
        moveLeft = 10;
        moveDown = ($(target).outerHeight());
    }, function() {
        var target = '#' + ($(this).attr('data-popbox'));
        $(target).hide();
    });
 
    $('a.popper').mousemove(function(e) {
        var target = '#' + ($(this).attr('data-popbox'));
         
        leftD = e.pageX + parseInt(moveLeft);
        maxRight = leftD + $(target).outerWidth();
        windowLeft = $(window).width() ;
        windowRight = 0;
        maxLeft = e.pageX - (parseInt(moveLeft) + $(target).outerWidth());
         
        if(maxRight > windowLeft && maxLeft > windowRight)
        {
            leftD = maxLeft;
        }
     
        topD = e.pageY - parseInt(moveDown);
        maxBottom = parseInt(e.pageY + parseInt(moveDown));
        windowBottom = parseInt(parseInt($(document).scrollTop()) + parseInt($(window).height()));
        maxTop = topD;
        windowTop = parseInt($(document).scrollTop());
        if(maxBottom > windowBottom)
        {
            topD = windowBottom - $(target).outerHeight();
        } else if(maxTop < windowTop){
            topD = windowTop;
        }
     
        $(target).css('top', topD).css('left', leftD);
     
     
    });	
});
  
  directionsDisplay.setMap(map);
  
}

function mesafeHesap()
{
	var mesafe, enKucuk = 0, gecici;
	
	mesafe = google.maps.geometry.spherical.computeDistanceBetween (lokasyon.position, markers[1].position);
	enKucuk = mesafe;
	enYakinSube = markers[1];
	for(var i = 1; i < markers.length; i++)
	{
		mesafe = google.maps.geometry.spherical.computeDistanceBetween (lokasyon.position, markers[i].position);
		//enKucuk = mesafe;
		if(mesafe < enKucuk)
		{
			enKucuk = mesafe;
			enYakinSube = markers[i];
		}
		
			
	}
	seciliSube = enYakinSube;
	calcRoute(enYakinSube.position);
	
	if(acKapa == 0)
		{
			$('.adres').toggle("slide");
			acKapa = 1;
		}
		
		var html = '<p style="color: black;">' + enYakinSube.adres + '</p>' ;

            new $.flavr({
                content         : html,
				position		: 'bottom-mid',
				closeEsc		: true,
				closeOverlay	: true,
				overlay			: true,
                animateEntrance : 'fadeInUpBig',
                animateClosing  : 'fadeOutRightBig',
                buttons         : {
                    kapat       : {}
                }
            });

}

function calcRoute(a) {
  var request = {
      origin:lokasyon.position,
      destination:a,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

google.maps.event.addDomListener(window, 'load', initialize);


function konumInfo(url){
    var html =  '<div id="pop1" class="popbox">'+
                '<h2>Konumum yanlış, Ne yapabilirim?</h2>' +
                '<p>* Otomatik olarak bulunan konumunuz hatalıysa, harita üzerinde kırmızı işareti <img src="'+ url +'/images/lokasyon.png" style="width: 21px; height: 29px;" /> doğru konuma sürükleyebilirsiniz.</p>' +
                '<h2>Konumum gösterilmiyor, Ne yapabilirim?</h2>' +
                '<p>* Otomatik olarak konumunuzun bulunabilmesi için tarayıcınızdan Konum ayarlarına izin vermelisiniz . <br/><img src="'+ url +'/images/izinver.png" width="100%" /><br/></p>';

    new $.flavr({
        content         : html,
        position		: 'bottom-mid',
        closeEsc		: true,
        closeOverlay	: true,
        overlay			: true,
        animateEntrance : 'fadeInUpBig',
        animateClosing  : 'fadeOutRightBig',
        buttons         : {
            kapat       : {}
        }
    });
}