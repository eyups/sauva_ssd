function submit_form(url)
{
	//Variable declaration and assignment
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var name = $("#name").val();
	var email = $("#email").val();
	var number = $("#number").val();
	var adres = $("#adres").val();
    var il = $("#il", '#second').val();
	var ilce = $("#ilce", '#second').val();
	var message = $("#message", '#second').val();
	var code = grecaptcha.getResponse();

	if( name == "" ) //Validation against empty field for fullname
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-danger" align="left">Lütfen tam isminizi gerekli alana doldurun. Teşekkürler.</div>');
		$("#name").focus();
	}
	else if( email == "" ) //Validation against empty field for email address
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-danger" align="left">Şikayetinizin işleme alınabilmesi için email adresi gereklidir. Teşekkürler.</div>');
		$("#email").focus();
	}
	else if(reg.test(email) == false) //Validation for working email address
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-danger" align="left">Üzgünüm, email adresiniz yanlış. Lütfen işleme alınabilmesi için geçerli bir email giriniz. Teşekkürler.</div>');
		$("#email").focus();
	}
	else if( number == "" ) //Validation against empty field for telephone number
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-danger" align="left">Şikayetinizin işleme alınabilmesi ve size geri dönüş için cep telefonunuz gereklidir. Teşekkürler.</div>');
		$("#number").focus();
	}
	else if( message == "" ) //Validation against empty field for email message
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-danger" align="left">Lütfen gerekli alana mesajınızı yazınız. Teşekkürler.</div>');
		$("#message", '#second').focus();
	}
	else if( code == "" ) //Validation against empty field for security captcha code
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-danger" align="left">Lütfen Güvenlik sorusunu yanıtlayınız. Teşekkürler.</div>');
		$("g-recaptcha-response").focus();
	}
	else
	{
		var dataString = 'name=' + name + '&email=' + email + '&number=' + number + '&message=' + message  + '&adres=' + adres + '&ilce=' + ilce + '&il=' + il +  '&g-recaptcha-response=' + code + '&submitted=1';
		$.ajax({
			type: "POST",
			url: url+"/SikayetMemnuniyet/sikayetGonder",
			data: dataString,
			beforeSend: function()
			{
				//Show loading image
				$("#response_brought").html('<br clear="all"><div class="alert alert-info">Lütfen bekleyin    <img src="'+ url +'/public/images/loading2.gif" alt="Gönderiliyor...." align="absmiddle" title="Gönderiliyor...."/></div>');

			},
			success: function(response)
			{
				//Check to see if the message is sent or not
				var response_brought = response.indexOf('Tebrikler');
				if( response_brought == -1 )
				{
					 $("#response_brought").html(response);
					 grecaptcha.reset();
				}
				else
				{
					 //Clear all form fields on success
					$("#name").val('');
					$("#email").val('');
					$("#number").val('');
					$("#sikayetTipi").val('');
					$("#adres").val('');
					$("#message", '#second').val('');
					$("#ilce", '#second').val('');
					$("#il", '#second').val('');
					$("#code").val('');
					grecaptcha.reset();
					//Display success message if the message is sent
					$("#response_brought").html(response);

					//Remove the success message also after a while of displaying the message to the user
					setTimeout(function() {
						$("#response_brought").html('');
					},10000);
				}
			}
		});
	}
}
