/********************************************************************************************************************
* Contact Form with Captcha using Ajax, Jquery and PHP
* This script is brought to you by Vasplus Programming Blog by whom all copyrights are reserved.
* Website: www.vasplus.info
* Email: vasplusblog@gmail.com or info@vasplus.info
* Please, this script must not be sold and do not remove this information from the top of this page.
*********************************************************************************************************************/



//This is the JS function that sends the mail - It is called when you click on the submit button which is in the form
function submit_form(url)
{
	//Variable declaration and assignment
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var prestijNo = $("#prestijNo").val();
	var vpb_captcha_code = grecaptcha.getResponse();


	if( prestijNo == "" ) //Validation against empty field for fullname
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">Lütfen prestij kartınızı gerekli alana giriniz.</div>');
		$("#name").focus();
	}
	else if( vpb_captcha_code == "" ) //Validation against empty field for security captcha code
	{
		$("#response_brought").html('<br clear="all"><div class="alert alert-warning" align="left">ütfen Güvenlik sorusunu yanıtlayınız. Teşekkürler.</div>');
		$("#code").focus();
	}
	else
	{
		var dataString = 'prestijNo=' + prestijNo + '&g-recaptcha-response=' + vpb_captcha_code + '&submitted=1';
		$.ajax({
			type: "POST",
			url: url+"/PrestijKart/sorgula",
			data: dataString,
			beforeSend: function()
			{
				//Show loading image
				$("#response_brought").html('<br clear="all"><div class="alert alert-info">Lütfen bekleyin    <img src="'+ url +'/public/images/loading2.gif" alt="Gönderiliyor...." align="absmiddle" title="Gönderiliyor...."/></div>');

			},
			success: function(response)
			{
				//Check to see if the message is sent or not
				var response_brought = response.indexOf('Sayın');
				if( response_brought == -1 )
				{
					//Display error message is the message is not sent
					 $("#response_brought").html(response);
					 grecaptcha.reset();
				}
				else
				{
					 //Clear all form fields on success
					$("#prestijNo").val('');
					$("#code").val('');

					//Display success message if the message is sent
					$("#response_brought").html(response);
					grecaptcha.reset();
					//Remove the success message also after a while of displaying the message to the user
					setTimeout(function() {
						$("#response_brought").html('');
					},30000);
				}
			}
		});
	}
}
