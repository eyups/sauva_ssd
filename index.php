<?php

//Türkçe karakter problemi için
header('Content-type: text/html; charset=utf-8');

// Sistem Dosyalarını otomatik include etmeye yarar
function __autoload($className){
    // Varlığının kontrolü yapılmazsa otherClasses'a koyduğum classlarla çakışabiliyor.
    if(file_exists('system/libs/' . $className .'.php')) {
        include_once 'system/libs/' . $className .'.php';
    }else
        return false;

}

// Config dosyasını include ediyoruz.
include_once 'app/config/config.php';


//Bootstrap Bölümü
$boot = new Bootstrap();

?>