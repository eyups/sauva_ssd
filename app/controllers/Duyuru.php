<?php

class Duyuru extends Controller{

    public function __construct() {
        parent::__construct();
    }

    public function index($pageNo = 1){

        //if(!empty($pageNo) && is_numeric($pageNo)) {

        if(!empty($pageNo)) {
            $duyuru_model           =   $this->load->model("Duyuru");
            $this->data["sayfaBilgisi"]   =   "Duyuru";
            $this->data["sayfaBaslik"]    =   "Duyurular";
            $this->data["menuListe"]      =   $duyuru_model->menuGetir();
            $this->data["toplamSayi"]     =   $duyuru_model->duyuruSayisiGetir();


            // Bir Sayfada kaç tane içerik olacağını belirliyoruz.
            $sayfaSayi = 4;

            $toplamSayi =   $this->data["toplamSayi"][0]["toplam"];
            $toplamSayfa = ceil($toplamSayi / $sayfaSayi);

            $this->data["toplamSayi"]     =   $toplamSayi;
            $this->data["toplamSayfa"]    =   $toplamSayfa;
            $this->data["sayfaNo"]        =   $pageNo;

            if($pageNo < 1)
                $sayfa = 1;

            if($pageNo > $toplamSayfa)
                $pageNo = $toplamSayfa;

            // SQL Sorgusunun Limitini belirliyoruz
            $limit = ($pageNo - 1) * $sayfaSayi;

            $this->data["duyuruListe"]    =   $duyuru_model->duyuruListGetir($limit, $sayfaSayi);

            $this->load->view("Site/header", $this->data);
            $this->load->view("Site/duyuru", $this->data);
            $this->load->view("Site/footer", $this->data);

            unset($this->data);

        }else{
            echo "yok";
        }
    }

    public function Detay($id = null){


        //if(!empty($id) && is_numeric($id)) {
        if(!empty($id)) {
            $duyuru_model           =   $this->load->model("Duyuru");
            //duyuruGetir($where)

            $where  =   array(
              "id"  =>  $id
            );

            $where2  =   array(
                "ann_id"  =>  $id
            );

            $this->data["sayfaBilgisi"]   =   "Duyuru";
            $this->data["menuListe"]      =   $duyuru_model->menuGetir();



            $this->data["duyuru"]         =   $duyuru_model->duyuruGetir($where);
			if(array_filter($this->data["duyuru"])){
				$this->data["duyuru"][0]["title"] = str_replace(array(".", "!"),"",mb_strtoupper($this->data["duyuru"][0]["title"]));
				$this->data["sayfaBaslik"]    =    $this->data["duyuru"][0]["title"];
				$this->data["duyuruImages"]         =   $duyuru_model->duyuruImagesGetir($where2);
				$this->load->view("Site/header", $this->data);
                $this->load->view("Site/duyuruDetay", $this->data);
                $this->load->view("Site/footer", $this->data);
			}else{
                 $this->load->view("Site/404");
            }
			            
            
        }else{
             $this->load->view("Site/404");	
            // 404 Hatası Aranılan sayfa bulunamadı.
        }
    }

}
