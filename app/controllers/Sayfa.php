<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 14.6.2015
 * Time: 12:26
 */

class Sayfa extends Controller {

    public function __construct(){
        parent::__construct();
    }

    public function Index(){
        echo "index";
    }

    public function sayfaGoster($param){

        if(empty($param))
            exit();

        //:
        $array = array(
            "short_name" => $param
        );

        $sayfa_model = $this->load->model("Sayfa");

        $this->data["sayfaBilgisi"]   =   "Sayfa";

        $result = $sayfa_model->pageControl($array);

        if($result == false)
        {
            // Sayfa Bulunamadı
            $this->load->view("Site/404");
            unset($this->data);
        }
        else{
            $this->data["menuListe"] = $sayfa_model->menuGetir();
            $this->data["deneme"] = $result[0];

            $this->data["sayfaBaslik"]    =   $result[0]["name"];



            $this->load->view("Site/header", $this->data);
            $this->load->view("Site/page", $this->data);
            $this->load->view("Site/footer", $this->data);
            unset($this->data);
        }



    }

} 