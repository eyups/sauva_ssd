<?php

class Index extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function anasayfa(){
        $index_model = $this->load->model("Index");
        $this->data["sayfaBilgisi"]   =   "Index";
        $this->data["sayfaBaslik"]    =   "Anasayfa";

        $this->data["sliderListe"] = $index_model->sliderListele();

        $this->data["menuListe"] = $index_model->menuGetir();

        $this->data["kampanyaListe"] = $index_model->kampanyaListele();

        $this->data["duyuruListe"]    =   $index_model->duyuruListGetir();

        $this->load->view("Site/header", $this->data);
        $this->load->view("Site/slider", $this->data);
        $this->load->view("Site/anasayfaContent", $this->data);
        $this->load->view("Site/footer", $this->data);

        unset($this->data);
    }

    public function index(){
        $this->anasayfa();
    }

}
