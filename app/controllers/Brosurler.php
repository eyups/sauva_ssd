<?php

class Brosurler extends Controller{
    public function __construct() {
        parent::__construct();
    }

    public function index($pageNo = 1){
        if(!empty($pageNo) && is_numeric($pageNo)) {
            $brosur_model = $this->load->model("Brosurler");
            $this->data["sayfaBilgisi"]   =   "Brosurler";
            $this->data["sayfaBaslik"]    =   "Broşürler";
            $this->data["menuListe"] = $brosur_model->menuGetir();
            $this->data["toplamSayi"]     =   $brosur_model->brosurSayisiGetir();


            // Bir Sayfada kaç tane içerik olacağını belirliyoruz.
            $sayfaSayi = 5;

            $toplamSayi =   $this->data["toplamSayi"][0]["toplam"];
            $toplamSayfa = ceil($toplamSayi / $sayfaSayi);

            $this->data["toplamSayi"]     =   $toplamSayi;
            $this->data["toplamSayfa"]    =   $toplamSayfa;
            $this->data["sayfaNo"]        =   $pageNo;

            if($pageNo < 1)
                $sayfa = 1;

            if($pageNo > $toplamSayfa)
                $pageNo = $toplamSayfa;

            // SQL Sorgusunun Limitini belirliyoruz
            $limit = ($pageNo - 1) * $sayfaSayi;

            $this->data["brosurListe"]    =   $brosur_model->brosurListGetir($limit, $sayfaSayi);

            $this->load->view("Site/header", $this->data);
            $this->load->view("Site/brosurler", $this->data);
            $this->load->view("Site/footer", $this->data);

            unset($this->data);

        }else{
            echo "yok";
        }

    }

}
