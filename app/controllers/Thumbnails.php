<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 16.6.2015
 * Time: 09:46
 */

class Thumbnails extends Controller{

    public function __construct() {
        parent::__construct();
    }

    public function createThumb($id){
        if (isset($id)) {
            $thumb = $this->load->otherClasses("easyphpthumbnail");
            // Your full path to the images
            $dir = str_replace(chr(92),chr(47),getcwd()) . '/public/medyaResimler/';


            // Create the thumbnail
            $thumb -> Thumbsize = 10;
            $thumb -> Percentage = true;

            $thumb -> Createthumb($dir . basename($id));
        }
    }

    public function duyuruThumb($id){
        if (isset($id)) {
            $thumb = $this->load->otherClasses("easyphpthumbnail");
            // Your full path to the images
            $dir = str_replace(chr(92),chr(47),getcwd()) . '/public/duyuruResimler/';


            // Create the thumbnail
            $thumb -> Thumbsize = 20;
            $thumb -> Percentage = true;

            $thumb -> Createthumb($dir . basename($id));
        }
    }

    public function kampanyaThumb($id){
        if (isset($id)) {
            $thumb = $this->load->otherClasses("easyphpthumbnail");
            // Your full path to the images
            $dir = str_replace(chr(92),chr(47),getcwd()) . '/public/kampanyaResimler/';


            // Create the thumbnail
            $thumb -> Thumbsize = 20;
            $thumb -> Percentage = true;

            $thumb -> Createthumb($dir . basename($id));
        }
    }

    public function brosurThumb($id){
        if (isset($id)) {
            $thumb = $this->load->otherClasses("easyphpthumbnail");
            // Your full path to the images
            $dir = str_replace(chr(92),chr(47),getcwd()) . '/public/brosurResimler/';


            // Create the thumbnail
            $thumb -> Thumbsize = 20;
            $thumb -> Percentage = true;

            $thumb -> Createthumb($dir . basename($id));
        }
    }

    public function duyurular($id){
        if (isset($id)) {
            $thumb = $this->load->otherClasses("easyphpthumbnail");
            // Your full path to the images
            $dir = str_replace(chr(92),chr(47),getcwd()) . '/public/duyuruResimler/';


            // Create the thumbnail
            $thumb -> Thumbwidth    =   490;
            $thumb -> Thumbheight   =   249;


            $thumb -> Createthumb($dir . basename($id));
        }
    }


} 