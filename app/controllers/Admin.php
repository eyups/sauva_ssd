<?php
class Admin extends Controller{

    protected $site_salt="";

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->login();
    }

    public function login(){
        // Oturum Kontrolü
        Session::init();
        if(Session::get("login")== true){
            header("Location:". SITE_URL ."/Panel/index");
        }else{
            Session::destroy();
            $this->load->view("Admin/loginForm");
        }
    }

    public function doLogin(){

        $result = array(false, "");

        $return = array();

        $data = array(
            "k_adi" => $_POST["username"],
            "parola" => md5($_POST["password"])
        );

        // Veri tabanı işlemleri
        $admin_model = $this->load->model("Admin");
        $result = $admin_model->userControl($data);


        //$salted_hash = password_hash($_POST["password"].$this->site_salt.$result[0]["parola_tuz"], PASSWORD_DEFAULT);

        //$salted_hash = md5($_POST["password"]);

        //if(!password_verify($_POST["password"], $result[0]["parola"])){

        if(!$result[0]){
            //Yanlış bilgiler girilmiş.
            //header("Location:". SITE_URL ."/Admin/login");

            $result = array(
                "status" => "error",
                "message" => "Sifre Yanlis: ".ob_get_contents()
            );
        }else{
            Session::init();
            Session::set("login", true);
            Session::set("username", $result[1][0]["k_adi"]);
            Session::set("userId", $result[1][0]["id"]);
            Session::set("tip", $result[1][0]["tip"]);

            $result = array(
                "status" => "success",
                "message" => ob_get_contents()
            );
        }

        ob_end_clean();

        echo json_encode($result, JSON_UNESCAPED_UNICODE);

    }

    public function logout(){

        header("Location:". SITE_URL ."#logout");
    }


}

?>