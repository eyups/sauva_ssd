<?php

class Panel extends Controller{

    //protected $site_salt="h1a2d3i4m5A6V7M8";

    protected $site_salt="";

    public function __construct(){
        parent::__construct();

        // Oturum Kontrolü
        Session::init();
        if(Session::get("login")== false){
            Session::destroy();
            header("Location:". SITE_URL ."/Admin/login");
        }

    }

    public function index(){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );
        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/content");
        $this->load->view("Panel/footer");
    }

    public function anasayfaMedyaEkle()
    {
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/anasayfaMedyaEkle");
        $this->load->view("Panel/footer");

    }

    public function anasayfaMedyaEdit($id)
    {
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_KAYANYAZI, $id);


        // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
        $edit = array_filter($edit);


        if(!empty($edit))
        {
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/anasayfaMedyaEdit", $edit);
            $this->load->view("Panel/footer");
        }


    }

    public function anasayfaMedyaList(){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $data["sliderListe"] = $panel_model->icerikListele(T_KAYANYAZI, "rank" , "ASC");

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/anasayfaMedyaList", $data);
        $this->load->view("Panel/footer");
    }

    public function uploadImage()
    {
        if(isset($_FILES["resim"])){
            $form = $this->load->otherClasses("Form");

            $form->post('name')
                ->isEmpty()
                ->length(0, 25);

            $form->resim('resim')
                ->isImage()
                ->checkSize()
                ->checkRes(1600, 514);

            if ($form->submit()) {



                $kaynak = $_FILES['resim']['tmp_name'];
                $resim = $_FILES['resim']['name'];
                $uzanti = substr($resim, strrpos($resim, '.') + 1);
                $yeniAd = substr(uniqid(md5(rand())), 0, 20) . '.' . $uzanti;
                $hedef = '/medyaResimler/';
                $hedefKok = ROOT_PUBLIC . $hedef;



                $name   =   $_POST["name"];
                $effect   =   $_POST["effect"];

                if(move_uploaded_file($kaynak, $hedefKok . $yeniAd))
                {

                    $panel_model    =   $this->load->model("Panel");

                    $getMaxRank["deneme"] = $panel_model->getMaxVal(T_KAYANYAZI, "rank");


                    $maxRank    =   intval($getMaxRank["deneme"][0]["rank"]);

                    $data   =   array(
                        "name"      =>  $name,
                        "url"       =>  "/medyaResimler/" . $yeniAd,
                        "effect"    =>  $effect,
                        "rank"      => $maxRank + 1
                    );

                    $data["remResult"]  = $panel_model->icerikEkle(T_KAYANYAZI, $data);

                    $data["headerInfo"] = array(
                        "username" => Session::get("username"),
                    );

                    $data["sliderListe"] = $panel_model->icerikListele(T_KAYANYAZI, "rank" , "ASC");

                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/anasayfaMedyaList", $data);
                    $this->load->view("Panel/footer");

                }
                else{
                    echo "Dosya yüklemede hata";
                }

            }
            else {
                $data["headerInfo"] = array(
                    "username" => Session::get("username"),
                );
                $errors["formErrors"] = $form->errors;

                $this->load->view("Panel/header", $data);
                $this->load->view("Panel/left");
                $this->load->view("Panel/anasayfaMedyaEkle", $errors);
                $this->load->view("Panel/footer");

            }
        }

    }

    public function doAnasayfaMedyaEdit($id){
        $form = $this->load->otherClasses("Form");

        $form->post('name')
            ->isEmpty()
            ->length(0, 25);

        if ($form->submit()) {
            $data = array(
                "name"              =>  $_POST["name"],
                "effect"        =>  $_POST["effect"]
            );


            $panel_model    =   $this->load->model("Panel");

            $data["remResult"]  =   $panel_model->icerikGuncelle(T_KAYANYAZI, $data, "id = " . $id);

            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $data["sliderListe"] = $panel_model->icerikListele(T_KAYANYAZI, "rank" , "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/anasayfaMedyaList", $data);
            $this->load->view("Panel/footer");

        }else{
            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $errors["formErrors"] = $form->errors;

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/anasayfaMedyaEdit/" . $id, $errors);
            $this->load->view("Panel/footer");
        }


    }

    public function anasayfaMedyaRemove($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $data["remResult"]    =   $panel_model->sliderRemove($id);

        $data["sliderListe"] = $panel_model->icerikListele(T_KAYANYAZI, "rank", "ASC");

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/anasayfaMedyaList", $data);
        $this->load->view("Panel/footer");

    }

    public function medyaUp($id){

        $panel_model    =   $this->load->model("Panel");

        $alttaki       =   $panel_model->upDownFunk(T_KAYANYAZI, "where id = $id");

        $ustteki           =   $panel_model->upDownFunk(T_KAYANYAZI, "where newRank = " . (intval($alttaki[0]["newRank"]) - 1));


        $ustteki    =   array_filter($ustteki);

        if(!empty($ustteki)){
            $yukselt           =   array(
                "rank"    =>  $ustteki[0]["rank"]
            );

            $indir           =   array(
                "rank"    =>  $alttaki[0]["rank"]
            );

            $data["remResult"]    = $panel_model->icerikGuncelle(T_KAYANYAZI, $yukselt, "id = $id");
            $data["remResult"]    = $panel_model->icerikGuncelle(T_KAYANYAZI, $indir, "id = " . $ustteki[0]["id"]);

            $data["sliderListe"] = $panel_model->icerikListele(T_KAYANYAZI, "rank", "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/anasayfaMedyaList", $data);
            $this->load->view("Panel/footer");
        }else{
            print_r($alttaki);
            print_r($ustteki);
            //header("Location: " . SITE_URL . "/Panel/anasayfaMedyaList");
        }
    }

    public function medyaDown($id){
        $panel_model    =   $this->load->model("Panel");

        $ustteki       =   $panel_model->upDownFunk(T_KAYANYAZI, "where id = $id");

        $alttaki          =   $panel_model->upDownFunk(T_KAYANYAZI, "where newRank = " . (intval($ustteki[0]["newRank"]) + 1));

        $alttaki    =   array_filter($alttaki);

        if(!empty($alttaki)){
            $yukselt           =   array(
                "rank"    =>  $alttaki[0]["rank"]
            );

            $indir           =   array(
                "rank"    =>  $ustteki[0]["rank"]
            );

            $data["remResult"]  =   $panel_model->icerikGuncelle(T_KAYANYAZI, $indir, "id = " . $alttaki[0]["id"]);
            $data["remResult"]  =   $panel_model->icerikGuncelle(T_KAYANYAZI, $yukselt, "id = $id");

            $data["sliderListe"] = $panel_model->icerikListele(T_KAYANYAZI, "rank", "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/anasayfaMedyaList", $data);
            $this->load->view("Panel/footer");
        }else{
            header("Location: " . SITE_URL . "/Panel/anasayfaMedyaList");
        }
    }

    public function sayfaList(){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $sayfa["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/sayfaList", $sayfa);
        $this->load->view("Panel/footer");

    }

    public function sayfaRemove($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $data["remResult"]    =   $panel_model->icerikSil(T_SAYFALAR, $id);

        $data["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/sayfaList", $data);
        $this->load->view("Panel/footer");

    }

    public function sayfaEkle()
    {
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/sayfaEkle");
        $this->load->view("Panel/footer");

    }

    public function sayfaEdit($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $data["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

        $data["toBeEdited"] = $panel_model->getToBeEdited(T_SAYFALAR, $id);

        // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
        $data = array_filter($data);

        if(!empty($data))
        {
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sayfaEdit", $data);
            $this->load->view("Panel/footer");
        }
    }

    public function doSayfaEkle(){
        $form = $this->load->otherClasses("Form");

        $form->post('name')
            ->isEmpty()
            ->length(0, 25);

        $form->post('content')
            ->isEmpty();

        if ($form->submit()) {

            // short_name için Türkçe karakter, karakter küçültme, boşluk çıkarma işlemleri
            // strtr => Türkçe Karakter, strtolower => Karakter Küçültme, str_replace => Boşluk Çıkartma
            $donustur   =   array(
                "ö" => "o",
                "Ö" => "O",
                "ç" => "c",
                "Ç" => "C",
                "ü" => "u",
                "Ü" => "U",
                "Ğ" => "g",
                "ğ" => "g",
                "ı" => "i",
                "İ" => "I",
                "ş" => "s",
                "Ş" => "S"
            );
            $short_name =   str_replace(" ", "" ,strtolower(strtr($_POST["name"], $donustur)));
            // short_name için Türkçe karakter, karakter küçültme, boşluk çıkarma işlemleri BİTTİ

            $panel_model = $this->load->model("Panel");

            $getMaxRank["deneme"] = $panel_model->getMaxVal(T_SAYFALAR, "rank");


            $maxRank    =   intval($getMaxRank["deneme"][0]["rank"]);

            $data = array(
                "name"              =>  $_POST["name"],
                "short_name"        =>  $short_name,
                "content"           =>  $_POST["content"],
                "ust_id"            =>  $_POST["ust_id"],
                "rank"              =>  $maxRank + 1, // Max'ın 1 fazlası olması gerektiği için
                "anamenu_visible"   =>  $_POST["anamenu"]
            );

            $data["remResult"]    =   $panel_model->icerikEkle(T_SAYFALAR, $data);

            $data["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sayfaList", $data);
            $this->load->view("Panel/footer");

        }else{
            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $data["formErrors"] = $form->errors;

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sayfaEkle", $data);
            $this->load->view("Panel/footer");
        }

    }

    public function doSayfaEdit($id){

        $form = $this->load->otherClasses("Form");

        $form->post('name')
            ->isEmpty()
            ->length(0, 25);

        $form->post('content')
            ->isEmpty();

        if ($form->submit()) {
            // short_name için Türkçe karakter, karakter küçültme, boşluk çıkarma işlemleri
            // strtr => Türkçe Karakter, strtolower => Karakter Küçültme, str_replace => Boşluk Çıkartma
            $donustur   =   array(
                "ö" => "o",
                "Ö" => "O",
                "ç" => "c",
                "Ç" => "C",
                "ü" => "u",
                "Ü" => "U",
                "Ğ" => "g",
                "ğ" => "g",
                "ı" => "i",
                "İ" => "I",
                "ş" => "s",
                "Ş" => "S"
            );
            $short_name =   str_replace(" ", "" ,strtolower(strtr($_POST["name"], $donustur)));
            // short_name için Türkçe karakter, karakter küçültme, boşluk çıkarma işlemleri BİTTİ

            $panel_model = $this->load->model("Panel");

            $edit["toBeEdited"] = $panel_model->getToBeEdited(T_SAYFALAR, $id);

            $curRank    =   intval($edit["toBeEdited"][0]["rank"]);

            $data = array(
                "name"              =>  $_POST["name"],
                "short_name"        =>  $short_name,
                "content"           =>  $_POST["content"],
                "ust_id"            =>  $_POST["ust_id"],
                "rank"              =>  $curRank, // Max'ın 1 fazlası olması gerektiği için
                "anamenu_visible"   =>  $_POST["anamenu"]
            );

            $data["remResult"]    =   $panel_model->icerikGuncelle(T_SAYFALAR, $data , "id = $id");

            $data["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sayfaList", $data);
            $this->load->view("Panel/footer");


        }else{
            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $panel_model = $this->load->model("Panel");

            $edit["toBeEdited"] = $panel_model->getToBeEdited(T_SAYFALAR, $id);

            // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
            $edit = array_filter($edit);

            if(!empty($edit))
            {
                $this->load->view("Panel/header", $data);
                $this->load->view("Panel/left");
                $this->load->view("Panel/sayfaEdit", $edit);
                $this->load->view("Panel/footer");
            }
        }

    }



    public function sayfaUp($id){

        $panel_model    =   $this->load->model("Panel");

        $alttaki       =   $panel_model->upDownFunk(T_SAYFALAR, "where id = $id");

        $ustteki           =   $panel_model->upDownFunk(T_SAYFALAR, "where newRank = " . (intval($alttaki[0]["newRank"]) - 1));


        $ustteki    =   array_filter($ustteki);

        if(!empty($ustteki)){
            $yukselt           =   array(
                "rank"    =>  $ustteki[0]["rank"]
            );

            $indir           =   array(
                "rank"    =>  $alttaki[0]["rank"]
            );

            $data["remResult"]    = $panel_model->icerikGuncelle(T_SAYFALAR, $yukselt, "id = $id");
            $data["remResult"]    = $panel_model->icerikGuncelle(T_SAYFALAR, $indir, "id = " . $ustteki[0]["id"]);

            $data["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sayfaList", $data);
            $this->load->view("Panel/footer");
        }else{
            header("Location: " . SITE_URL . "/Panel/sayfaList");
        }




    }

    public function sayfaDown($id){
        $panel_model    =   $this->load->model("Panel");

        $ustteki       =   $panel_model->upDownFunk(T_SAYFALAR, "where id = $id");

        $alttaki          =   $panel_model->upDownFunk(T_SAYFALAR, "where newRank = " . (intval($ustteki[0]["newRank"]) + 1));

        $alttaki    =   array_filter($alttaki);

        if(!empty($alttaki)){
            $yukselt           =   array(
                "rank"    =>  $alttaki[0]["rank"]
            );

            $indir           =   array(
                "rank"    =>  $ustteki[0]["rank"]
            );

            $data["remResult"]  =   $panel_model->icerikGuncelle(T_SAYFALAR, $indir, "id = " . $alttaki[0]["id"]);
            $data["remResult"]  =   $panel_model->icerikGuncelle(T_SAYFALAR, $yukselt, "id = $id");

            $data["sayfaListe"] = $panel_model->icerikListele(T_SAYFALAR, "rank", "ASC");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sayfaList", $data);
            $this->load->view("Panel/footer");
        }else{
            header("Location: " . SITE_URL . "/Panel/sayfaList");
        }
    }

    public function duyuruEkle(){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/duyuruEkle");
        $this->load->view("Panel/footer");
    }

    public function duyuruList(){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $sayfa["duyuruListe"] = $panel_model->icerikListele(T_DUYURULAR, "date", "DESC");

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/duyuruList", $sayfa);
        $this->load->view("Panel/footer");
    }

    public function doDuyuruEkle()
    {

        $this->redirectIfNotAdmin();

        $sayac  =   0;
        if (isset($_FILES["resim"])) {
            $form = $this->load->otherClasses("Form");

            $form->post('title')
                ->isEmpty()
                ->length(0, 75);

            $form->post('short_content')
                ->isEmpty();

            $form->post('content')
                ->isEmpty();

            $form->resim('resim')
                ->mul_isImage()
                ->mul_checkSize()
                ->mul_checkRes(1140, 550);


            if ($form->submit()) {

                date_default_timezone_set('Europe/Istanbul');
                $date = date('Y/m/d h:i:s', time());

                $data   =   array(
                    "title"         =>  $_POST["title"],
                    "short_content" =>  $_POST["short_content"],
                    "content"       =>  $_POST["content"],
                    "published"     =>  $_POST["published"],
                    "date"          =>  $date
                );

                $panel_model    =   $this->load->model("Panel");

                $data["remResult"]  =   $panel_model->icerikEkle(T_DUYURULAR, $data);

                if($data["remResult"][0])
                {
                    $getMaxRank     =   $panel_model->getMaxVal(T_DUYURULAR, "id");
                    $maxID          =   intval($getMaxRank[0]["id"]);

                    foreach ($_FILES['resim']['name'] as $f => $name) {
                        $kaynak = $_FILES['resim']['tmp_name'][$f];
                        $resim = $_FILES['resim']['name'][$f];
                        $uzanti = substr($resim, strrpos($resim, '.') + 1);
                        $yeniAd = substr(uniqid(md5(rand())), 0, 20) . '.' . $uzanti;
                        $hedef = '/duyuruResimler/';
                        $hedefKok = ROOT_PUBLIC . $hedef;

                        if (move_uploaded_file($kaynak, $hedefKok . $yeniAd)) {
                            if($sayac == 0)
                            {
                                $img   =   array(
                                    "ann_id"            =>  $maxID,
                                    "url"               =>  "/duyuruResimler/" . $yeniAd,
                                    "is_default_pic"    =>  1
                                );
                            }else{
                                $img   =   array(
                                    "ann_id"      =>  $maxID,
                                    "url"       =>  "/duyuruResimler/" . $yeniAd
                                );
                            }

                            $result = $panel_model->icerikEkle(T_DUYURULAR_IMG, $img);
                            if($result[0]){
                                $sayac++;
                            }
                            else{
                                echo "Duyuru resimleri yüklerken hata oluştu.";
                            }
                        } else {
                            echo "Dosya yüklemede hata";
                        }
                    }

                    if($sayac   ==  count($_FILES['resim']['name']))
                    {
                        $data["headerInfo"] = array(
                            "username" => Session::get("username"),
                        );

                        $data["duyuruListe"] = $panel_model->icerikListele(T_DUYURULAR);

                        $this->load->view("Panel/header", $data);
                        $this->load->view("Panel/left");
                        $this->load->view("Panel/duyuruList", $data);
                        $this->load->view("Panel/footer");
                    }

                }
                else{
                    echo "İçerik eklemede hata";
                }




            }else
            {
                $data["headerInfo"] = array(
                    "username" => Session::get("username"),
                );

                $data["beforeErr"]   =   array(
                    "title"         =>  $_POST["title"],
                    "short_content" =>  $_POST["short_content"],
                    "content"       =>  $_POST["content"],
                    "published"     =>  $_POST["published"],
                    "resim "        =>  $_FILES['resim']['name']
                );

                $data["formErrors"] = $form->errors;

                $this->load->view("Panel/header", $data);
                $this->load->view("Panel/left");
                $this->load->view("Panel/duyuruEkle", $data);
                $this->load->view("Panel/footer");
            }
        }
    }

    public function duyuruRemove($id){

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $data["remResult"]     =   $panel_model->duyuruRemove($id);
        $data["duyuruListe"]   =   $panel_model->icerikListele(T_DUYURULAR);

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/duyuruList", $data);
        $this->load->view("Panel/footer");
    }

    public function duyuruEdit($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $edit["duyuruListe"] = $panel_model->icerikListele(T_DUYURULAR, $id);

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_DUYURULAR, $id);

        $edit["duyuruImages"] = $panel_model->getToBeEdited(T_DUYURULAR_IMG, $id, "ann_id");

        // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
        $edit = array_filter($edit);

        if(!empty($edit))
        {
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/duyuruEdit", $edit);
            $this->load->view("Panel/footer");
        }

    }

    public function removeDuyuruImage($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $duyuruImage = $panel_model->getToBeEdited(T_DUYURULAR_IMG, $id, "id");

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_DUYURULAR, $duyuruImage[0]["ann_id"]);

        $edit["remResult"]    =   $panel_model->duyuruImageRemove($id);

        header("Location: " . SITE_URL . "/Panel/duyuruEdit/" . $duyuruImage[0]["ann_id"]);

    }

    public function varsayilanYap($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $duyuruImage = $panel_model->getToBeEdited(T_DUYURULAR_IMG, $id, "id");

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_DUYURULAR, $duyuruImage[0]["ann_id"]);

        $data   =   array(
            "is_default_pic"     =>     0
        );

        $edit["remResult"]    =   $panel_model->icerikGuncelle(T_DUYURULAR_IMG, $data, "ann_id = " . $duyuruImage[0]["ann_id"]);

        $data   =   array(
            "is_default_pic"     =>     1
        );

        $edit["remResult"]    =   $panel_model->icerikGuncelle(T_DUYURULAR_IMG, $data, "id = $id");

        header("Location: " . SITE_URL . "/Panel/duyuruEdit/" . $duyuruImage[0]["ann_id"]);

    }

    public function doDuyuruEdit($id){
        $sayac  =   0;
        if (!empty($_POST["gonder"])) {
            $form = $this->load->otherClasses("Form");
            /*
            $form->post('title')
                ->isEmpty()
                ->length(0, 75);

            $form->post('short_content')
                ->isEmpty();

            $form->post('content')
                ->isEmpty();
            */

            /*if ($_FILES["resim"]["name"][0] != null) {
                $form->resim('resim')
                    ->mul_isImage()
                    ->mul_checkSize()
                    ->mul_checkRes(1140, 550);
            }*/

            if ($form->submit()) {

                $data   =   array(
                    "title"         =>  $_POST["title"],
                    "short_content" =>  $_POST["short_content"],
                    "content"       =>  $_POST["content"],
                    "published"     =>  $_POST["published"]
                );

                $panel_model    =   $this->load->model("Panel");

                $data["remResult"]         =   $panel_model->icerikGuncelle(T_DUYURULAR, $data, "id = $id");

                if($data["remResult"][0])
                {
                    if ($_FILES["resim"]["name"][0] != null) {
                        foreach ($_FILES['resim']['name'] as $f => $name) {
                            $kaynak = $_FILES['resim']['tmp_name'][$f];
                            $resim = $_FILES['resim']['name'][$f];
                            $uzanti = substr($resim, strrpos($resim, '.') + 1);
                            $yeniAd = substr(uniqid(md5(rand())), 0, 20) . '.' . $uzanti;
                            $hedef = '/duyuruResimler/';
                            $hedefKok = ROOT_PUBLIC . $hedef;

                            if (move_uploaded_file($kaynak, $hedefKok . $yeniAd)) {
                                $img = array(
                                    "ann_id" => $id,
                                    "url" => "/duyuruResimler/" . $yeniAd,
                                );
                                $result = $panel_model->icerikEkle(T_DUYURULAR_IMG, $img);
                                if ($result[0]) {
                                    $sayac++;
                                } else {
                                    echo "Duyuru resimleri yüklerken hata oluştu.";
                                }
                            } else {
                                echo "Dosya yüklemede hata";
                            }
                        }

                        if ($sayac == count($_FILES['resim']['name'])) {
                            $data["headerInfo"] = array(
                                "username" => Session::get("username"),
                            );

                            $data["duyuruListe"] = $panel_model->icerikListele(T_DUYURULAR);

                            $this->load->view("Panel/header", $data);
                            $this->load->view("Panel/left");
                            $this->load->view("Panel/duyuruList", $data);
                            $this->load->view("Panel/footer");
                        }
                    }else{
                        $data["headerInfo"] = array(
                            "username" => Session::get("username"),
                        );

                        $data["duyuruListe"] = $panel_model->icerikListele(T_DUYURULAR);

                        $this->load->view("Panel/header", $data);
                        $this->load->view("Panel/left");
                        $this->load->view("Panel/duyuruList", $data);
                        $this->load->view("Panel/footer");
                    }

                }
                else{
                    $data["headerInfo"] = array(
                        "username" => Session::get("username"),
                    );
                    $data["remResult"]  = false;
                    $data["duyuruListe"] = $panel_model->icerikListele(T_DUYURULAR);

                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/duyuruList", $data);
                    $this->load->view("Panel/footer");
                }

            }else
            {
                $data["headerInfo"] = array(
                    "username" => Session::get("username"),
                );

                $edit["formErrors"] = $form->errors;

                $panel_model = $this->load->model("Panel");

                $edit["toBeEdited"] = $panel_model->getToBeEdited(T_DUYURULAR, $id);

                $edit["duyuruImages"] = $panel_model->getToBeEdited(T_DUYURULAR_IMG, $id, "ann_id");

                // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
                $edit = array_filter($edit);

                if(!empty($edit))
                {
                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/duyuruEdit", $edit);
                    $this->load->view("Panel/footer");
                }
            }
        }

    }

    public function kampanyaEkle()
    {

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/kampanyaEkle");
        $this->load->view("Panel/footer");

    }

    public function kampanyaList(){

        $this->redirectIfNotAdmin();
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $data["kampanyaListe"] = $panel_model->icerikListele(T_KAMPANYALAR);

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/kampanyaList", $data);
        $this->load->view("Panel/footer");
    }

    public function kampanyaEdit($id)
    {
        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_KAMPANYALAR, $id);


        // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
        $edit = array_filter($edit);


        if(!empty($edit))
        {
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/kampanyaEdit", $edit);
            $this->load->view("Panel/footer");
        }


    }

    public function doKampanyaEkle()
    {

        $this->redirectIfNotAdmin();

        if(isset($_FILES["resim"])){
            $form = $this->load->otherClasses("Form");

            $form->post('name')
                ->isEmpty()
                ->length(0, 25);

            $form->post('ilk_tarih')
                ->isEmpty()
                ->length(0, 15);

            $form->post('son_tarih')
                ->isEmpty()
                ->length(0, 15);

            $form->resim('resim')
                ->isImage()
                ->checkSize()
                ->checkRes(400, 400);

            if ($form->submit()) {

                $kaynak = $_FILES['resim']['tmp_name'];
                $resim = $_FILES['resim']['name'];
                $uzanti = substr($resim, strrpos($resim, '.') + 1);
                $yeniAd = substr(uniqid(md5(rand())), 0, 20) . '.' . $uzanti;
                $hedef = '/kampanyaResimler/';
                $hedefKok = ROOT_PUBLIC . $hedef;



                $name    =   $_POST["name"];
                $ilk_tarih   =   $_POST["ilk_tarih"];
                $son_tarih   =   $_POST["son_tarih"];

                if(move_uploaded_file($kaynak, $hedefKok . $yeniAd))
                {
                    $panel_model    =   $this->load->model("Panel");
                    $data   =   array(
                        "name"      =>  $name,
                        "url"       =>  "/kampanyaResimler/" . $yeniAd,
                        "ilk_tarih"    =>  $ilk_tarih,
                        "son_tarih"    =>  $son_tarih
                    );
                    $data["remResult"]  =   $panel_model->icerikEkle(T_KAMPANYALAR, $data);

                    $data["headerInfo"] = array(
                        "username" => Session::get("username"),
                    );

                    $data["kampanyaListe"] = $panel_model->icerikListele(T_KAMPANYALAR);

                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/kampanyaList", $data);
                    $this->load->view("Panel/footer");


                }
                else{
                    echo "Dosya yüklemede hata";
                }

            }
            else {
                $data["headerInfo"] = array(
                    "username" => Session::get("username"),
                );
                $errors["formErrors"] = $form->errors;

                $this->load->view("Panel/header", $data);
                $this->load->view("Panel/left");
                $this->load->view("Panel/kampanyaEkle", $errors);
                $this->load->view("Panel/footer");

            }
        }

    }

    public function doKampanyaEdit($id){

        $this->redirectIfNotAdmin();

        $form = $this->load->otherClasses("Form");

        $form->post('name')
            ->isEmpty()
            ->length(0, 25);

        $form->post('ilk_tarih')
            ->isEmpty()
            ->length(0, 15);

        $form->post('son_tarih')
            ->isEmpty()
            ->length(0, 15);

        if ($form->submit()) {
            $name       =   $_POST["name"];
            $ilk_tarih  =   $_POST["ilk_tarih"];
            $son_tarih  =   $_POST["son_tarih"];

            $data   =   array(
                "name"      =>  $name,
                "ilk_tarih" =>  $ilk_tarih,
                "son_tarih" =>  $son_tarih
            );

            $panel_model    =   $this->load->model("Panel");

            $data["remResult"]  =   $panel_model->icerikGuncelle(T_KAMPANYALAR, $data, "id = " . $id);

            $data["kampanyaListe"] = $panel_model->icerikListele(T_KAMPANYALAR);

            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/kampanyaList", $data);
            $this->load->view("Panel/footer");

        }else{
            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $errors["formErrors"] = $form->errors;

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/kampanyaEdit/" . $id, $errors);
            $this->load->view("Panel/footer");
        }


    }

    public function kampanyaRemove($id){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $fileName = $panel_model->ozelListele(T_KAMPANYALAR, "WHERE id = $id", "ASC", null, "url");

        $data["remResult"]    =   $panel_model->icerikSil(T_KAMPANYALAR, $id);

        $data["kampanyaListe"] = $panel_model->icerikListele(T_KAMPANYALAR);

        unlink(ROOT_PUBLIC . $fileName[0]["url"] );

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/kampanyaList", $data);
        $this->load->view("Panel/footer");

    }

    public function brosurEkle(){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/brosurEkle");
        $this->load->view("Panel/footer");
    }

    public function brosurList(){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $sayfa["brosurListe"] = $panel_model->icerikListele(T_BROSURLER);

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/brosurList", $sayfa);
        $this->load->view("Panel/footer");
    }

    public function brosurEdit($id){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $edit["brosurListe"] = $panel_model->icerikListele(T_BROSURLER, $id);

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_BROSURLER, $id);

        $edit["brosurImages"] = $panel_model->getToBeEdited(T_BROSURLER_IMG, $id, "ins_id");

        // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
        $edit = array_filter($edit);

        if(!empty($edit))
        {
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/brosurEdit", $edit);
            $this->load->view("Panel/footer");
        }

    }

    public function doBrosurEkle()
    {

        $this->redirectIfNotAdmin();

        $sayac  =   0;
        if (isset($_FILES["resim"])) {
            $form = $this->load->otherClasses("Form");

            $form->post('title')
                ->isEmpty()
                ->length(0, 25);

            $form->post('ilk_tarih')
                ->isEmpty()
                ->length(0, 15);

            $form->post('son_tarih')
                ->isEmpty()
                ->length(0, 15);

            $form->resim('resim')
                ->mul_isImage()
                ->mul_checkSize()
                ->mul_checkRes(1240, 1754);

            if ($form->submit()) {

                $data   =   array(
                    "title"         =>  $_POST["title"],
                    "ilk_tarih" =>  $_POST["ilk_tarih"],
                    "son_tarih"       =>  $_POST["son_tarih"]
                );

                $panel_model    =   $this->load->model("Panel");
                $data["remResult"]         =   $panel_model->icerikEkle(T_BROSURLER, $data);

                if($data["remResult"][0])
                {
                    $getMaxRank     =   $panel_model->getMaxVal(T_BROSURLER, "id");
                    $maxID          =   intval($getMaxRank[0]["id"]);

                    foreach ($_FILES['resim']['name'] as $f => $name) {
                        $kaynak = $_FILES['resim']['tmp_name'][$f];
                        $resim = $_FILES['resim']['name'][$f];
                        $uzanti = substr($resim, strrpos($resim, '.') + 1);
                        $yeniAd = substr(uniqid(md5(rand())), 0, 20) . '.' . $uzanti;
                        $hedef = '/brosurResimler/';
                        $hedefKok = ROOT_PUBLIC . $hedef;

                        if (move_uploaded_file($kaynak, $hedefKok . $yeniAd)) {
                            $img   =   array(
                                "ins_id"      =>  $maxID,
                                "url"       =>  "/brosurResimler/" . $yeniAd,
                            );
                            $result = $panel_model->icerikEkle(T_BROSURLER_IMG, $img);
                            if($result[0]){
                                $sayac++;
                            }
                            else{
                                echo "Broşür resimleri yüklerken hata oluştu.";
                            }
                        } else {
                            echo "Dosya yüklemede hata";
                        }
                    }

                    if($sayac   ==  count($_FILES['resim']['name']))
                    {
                        $data["headerInfo"] = array(
                            "username" => Session::get("username"),
                        );
                        $data["brosurListe"] = $panel_model->icerikListele(T_BROSURLER);
                        $this->load->view("Panel/header", $data);
                        $this->load->view("Panel/left");
                        $this->load->view("Panel/brosurList", $data);
                        $this->load->view("Panel/footer");
                    }

                }
                else{
                    //İcerik Ekleme de hata
                    $data["headerInfo"] = array(
                        "username" => Session::get("username"),
                    );
                    $data["brosurListe"] = $panel_model->icerikListele(T_BROSURLER);
                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/brosurList", $data);
                    $this->load->view("Panel/footer");
                }
            }else
            {
                $data["headerInfo"] = array(
                    "username" => Session::get("username"),
                );

                $errors["formErrors"] = $form->errors;

                $this->load->view("Panel/header", $data);
                $this->load->view("Panel/left");
                $this->load->view("Panel/brosurEkle", $errors);
                $this->load->view("Panel/footer");
            }
        }
    }

    public function doBrosurEdit($id){

        $this->redirectIfNotAdmin();

        $sayac  =   0;
        if (!empty($_POST["gonder"])) {
            $form = $this->load->otherClasses("Form");
            $form->post('title')
                ->isEmpty()
                ->length(0, 25);

            $form->post('ilk_tarih')
                ->isEmpty()
                ->length(0, 15);

            $form->post('son_tarih')
                ->isEmpty()
                ->length(0, 15);

            if ($_FILES["resim"]["name"][0] != null) {
                $form->resim('resim')
                    ->mul_isImage()
                    ->mul_checkSize()
                    ->mul_checkRes(1240, 1754);
            }

            if ($form->submit()) {

                $data   =   array(
                    "title"         =>  $_POST["title"],
                    "ilk_tarih"     =>  $_POST["ilk_tarih"],
                    "son_tarih"     =>  $_POST["son_tarih"]
                );

                $panel_model    =   $this->load->model("Panel");

                $data["remResult"]  =   $panel_model->icerikGuncelle(T_BROSURLER, $data, "id = $id");

                if($data["remResult"][0])
                {
                    $getMaxID     =   $panel_model->getMaxVal(T_BROSURLER, "id");
                    $maxID          =   intval($getMaxID[0]["id"]);
                    if ($_FILES["resim"]["name"][0] != null) {
                        foreach ($_FILES['resim']['name'] as $f => $name) {
                            $kaynak = $_FILES['resim']['tmp_name'][$f];
                            $resim = $_FILES['resim']['name'][$f];
                            $uzanti = substr($resim, strrpos($resim, '.') + 1);
                            $yeniAd = substr(uniqid(md5(rand())), 0, 20) . '.' . $uzanti;
                            $hedef = '/brosurResimler/';
                            $hedefKok = ROOT_PUBLIC . $hedef;

                            if (move_uploaded_file($kaynak, $hedefKok . $yeniAd)) {
                                $img = array(
                                    "ins_id" => $maxID,
                                    "url" => "/brosurResimler/" . $yeniAd,
                                );
                                $result = $panel_model->icerikEkle(T_BROSURLER_IMG, $img);
                                if ($result[0]) {
                                    $sayac++;
                                } else {
                                    echo "Duyuru resimleri yüklerken hata oluştu.";
                                }
                            } else {
                                echo "Dosya yüklemede hata";
                            }
                        }

                        if ($sayac == count($_FILES['resim']['name'])) {
                            $data["headerInfo"] = array(
                                "username" => Session::get("username"),
                            );

                            $data["brosurListe"] = $panel_model->icerikListele(T_BROSURLER);
                            $this->load->view("Panel/header", $data);
                            $this->load->view("Panel/left");
                            $this->load->view("Panel/brosurList", $data);
                            $this->load->view("Panel/footer");
                        }
                    }else{
                        $data["headerInfo"] = array(
                            "username" => Session::get("username"),
                        );

                        $data["brosurListe"] = $panel_model->icerikListele(T_BROSURLER);

                        $this->load->view("Panel/header", $data);
                        $this->load->view("Panel/left");
                        $this->load->view("Panel/brosurList", $data);
                        $this->load->view("Panel/footer");
                    }

                }
                else{
                    $data["headerInfo"] = array(
                        "username" => Session::get("username"),
                    );

                    $edit["brosurListe"] = $panel_model->icerikListele(T_BROSURLER);
                    $edit["remResult"]  = false;

                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/brosurList", $edit);
                    $this->load->view("Panel/footer");
                }

            }else
            {
                $data["headerInfo"] = array(
                    "username" => Session::get("username"),
                );

                $edit["formErrors"] = $form->errors;

                $panel_model = $this->load->model("Panel");

                $edit["toBeEdited"] = $panel_model->getToBeEdited(T_BROSURLER, $id);

                $edit["brosurImages"] = $panel_model->getToBeEdited(T_BROSURLER_IMG, $id, "ins_id");

                // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
                $edit = array_filter($edit);

                if(!empty($edit))
                {
                    $this->load->view("Panel/header", $data);
                    $this->load->view("Panel/left");
                    $this->load->view("Panel/brosurEdit", $edit);
                    $this->load->view("Panel/footer");
                }
            }
        }

    }

    public function brosurRemove($id){

        $this->redirectIfNotAdmin();


        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $array["remResult"]     =   $panel_model->brosurRemove($id);
        $array["brosurListe"]   =   $panel_model->icerikListele(T_BROSURLER);

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/brosurList", $array);
        $this->load->view("Panel/footer");
    }

    public  function removeBrosurImage($id){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model = $this->load->model("Panel");

        $brosurImage = $panel_model->getToBeEdited(T_BROSURLER_IMG, $id, "id");

        $edit["toBeEdited"] = $panel_model->getToBeEdited(T_BROSURLER, $brosurImage[0]["ins_id"]);

        $edit["remResult"]    =   $panel_model->brosurImageRemove($id);

        header("Location: " . SITE_URL . "/Panel/brosurEdit/" . $brosurImage[0]["ins_id"]);

        // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
        $edit = array_filter($edit);

        if(!empty($edit))
        {
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/brosurEdit", $edit);
            $this->load->view("Panel/footer");
        }
    }


    public function sifreDegistir(){
        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/sifreDegistir");
        $this->load->view("Panel/footer");
    }

    public function changeSifre(){

        if($_POST["yeniSifre"] == $_POST["yeniSifreT"]){
            $data["headerInfo"] = array(
                "username" => Session::get("username"),
            );

            $salt_array = array(
                ":k_adi" => Session::get("username"),
            );

            $panel_model = $this->load->model("Panel");

            $result = $panel_model->tuzGetir($salt_array);

            //$salted_hash = password_hash($_POST["yeniSifre"].$this->site_salt.$result[0]["parola_tuz"], PASSWORD_DEFAULT);

            $salted_hash = md5($_POST["yeniSifre"]);

            $array = array(
                "parola" => $salted_hash
            );

            $data["remResult"] = $panel_model->icerikGuncelle(T_KULLANICILAR, $array, "id = 138598");

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sifreDegistir", $data);
            $this->load->view("Panel/footer");
            Session::destroy();
        }else{
            $data["isSame"] = 0;
            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/sifreDegistir", $data);
            $this->load->view("Panel/footer");
        }
    }

    public function siteSettings(){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );

        $panel_model    =   $this->load->model("Panel");

        $data["ayarListe"]  =   $panel_model->icerikListele(T_AYARLAR);

        $this->load->view("Panel/header", $data);
        $this->load->view("Panel/left");
        $this->load->view("Panel/siteSettings", $data);
        $this->load->view("Panel/footer");
    }

    public function saveSettings(){

        $this->redirectIfNotAdmin();

        $data["headerInfo"] = array(
            "username" => Session::get("username"),
        );


        $form = $this->load->otherClasses("Form");

        $form->post('facebook')
            ->isEmpty();

        $form->post('twitter')
            ->isEmpty();

        $form->post('site_title')
            ->isEmpty();

        $form->post('email')
            ->isEmpty();

        $form->post('phone')
            ->isEmpty();

        $form->post('footer_text')
            ->isEmpty();

        if($form->submit()){

            $array = array(
                "facebook"      =>    $_POST["facebook"],
                "twitter"       =>    $_POST["twitter"],
                "site_title"    =>    $_POST["site_title"],
                "email"         =>    $_POST["email"],
                "phone"         =>    $_POST["phone"],
                "footer_text"   =>    $_POST["footer_text"],
            );
            $sayac = 0;
            $panel_model    =   $this->load->model("Panel");
            foreach($array as $key => $value){
                $temp = array(
                    "val" => $value
                );
                $result = $panel_model->icerikGuncelle(T_AYARLAR, $temp , "opt = '" . $key . "'");
                if($result[0])
                    $sayac++;
            }

            if($sayac == 6)
                $data["remResult"] = true;
            else
                $data["remResult"] = false;

            $data["ayarListe"]  =   $panel_model->icerikListele(T_AYARLAR);

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/siteSettings", $data);
            $this->load->view("Panel/footer");


        }else{
            $data["formErrors"] =   $form->errors;

            $panel_model    =   $this->load->model("Panel");

            $data["ayarListe"]  =   $panel_model->icerikListele(T_DUYURULAR_IMG);

            $this->load->view("Panel/header", $data);
            $this->load->view("Panel/left");
            $this->load->view("Panel/siteSettings", $data);
            $this->load->view("Panel/footer");
        }

    }

    public function redirectIfNotAdmin()
    {
        if (!(Session::get("tip") == 1 )) {
            echo "Bu sayfayı görme yetkiniz yok";
            exit;
        }
    }

}