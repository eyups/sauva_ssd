<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 14.6.2015
 * Time: 13:15
 */

class Duyuru_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function duyuruListGetir($limit, $sayfaSayi){
        $sql = "SELECT ". T_DUYURULAR .".id, title, short_content, content, published, date, url FROM ". T_DUYURULAR ."
        INNER JOIN ". T_DUYURULAR_IMG ."  ON ". T_DUYURULAR .".id = ". T_DUYURULAR_IMG .".ann_id AND published = 1 AND is_default_pic = 1 GROUP BY ". T_DUYURULAR .".id ORDER BY date DESC LIMIT $limit,$sayfaSayi";

        return $this->db->select($sql);
    }

    public function duyuruGetir($where){
        $sql = "SELECT * FROM ". T_DUYURULAR ." WHERE id = :id";
        $count = $this->db->affectedRows($sql, $where);
        if($count > 0){
            return $this->db->select($sql, $where);
        }else{
            return false;
        }
        
    }

    public function duyuruImagesGetir($where){
        $sql = "SELECT * FROM ". T_DUYURULAR_IMG ." WHERE ann_id = :ann_id";
        return $this->db->select($sql, $where);
    }

    public function duyuruSayisiGetir(){
        $sql = "SELECT COUNT(*) AS toplam FROM ". T_DUYURULAR ." WHERE published = 1 ORDER BY date ASC";


        return $this->db->select($sql);
    }

    public function menuGetir(){
        $sql = "SELECT * FROM ". T_SAYFALAR ." ORDER BY rank ASC";
        return $this->db->select($sql);
    }
} 