<?php

class Panel_Model extends Model{
    public function __construct() {
        parent::__construct();
    }

    public function sliderRemove($where){

        $array= array(
            ":id" => $where
        );

        $sql = "SELECT * FROM ". T_KAYANYAZI ." WHERE id = :id";
        $sth = $this->db->select($sql, $array);


        $fileName = $sth[0]["url"];

        unlink(ROOT_PUBLIC . $fileName );

        return $this->db->delete(T_KAYANYAZI, "id = " .  $where);
    }

    public function brosurImageRemove($where){

        $array= array(
            ":id" => $where
        );

        $sql = "SELECT * FROM ". T_BROSURLER_IMG ." WHERE id = :id";
        $sth = $this->db->select($sql, $array);


        $fileName = $sth[0]["url"];

        unlink(ROOT_PUBLIC . $fileName );



        return $this->db->delete(T_BROSURLER_IMG, "id = " .  $where);
    }

    public function brosurRemove($where){

        $array= array(
            ":id" => $where
        );

        $sql = "SELECT * FROM ". T_BROSURLER_IMG ." WHERE ann_id = :id";
        $sth = $this->db->select($sql, $array);

        for($i = 0 ; $i < count($sth); $i++) {
            $fileName = $sth[$i]["url"];

            unlink(ROOT_PUBLIC . $fileName);
        }

        return $this->db->delete(T_BROSURLER, "id = " .  $where);
    }

    public function duyuruImageRemove($where){

        $array= array(
            ":id" => $where
        );

        $sql = "SELECT * FROM ". T_DUYURULAR_IMG ." WHERE id = :id";
        $sth = $this->db->select($sql, $array);


        $fileName = $sth[0]["url"];

        unlink(ROOT_PUBLIC . $fileName );



        return $this->db->delete(T_DUYURULAR_IMG, "id = " .  $where);
    }

    public function duyuruRemove($where){

        $array= array(
            ":id" => $where
        );

        $sql = "SELECT * FROM ". T_DUYURULAR_IMG ." WHERE ann_id = :id";
        $sth = $this->db->select($sql, $array);

        for($i = 0 ; $i < count($sth); $i++) {
            $fileName = $sth[$i]["url"];

            unlink(ROOT_PUBLIC . $fileName);
        }

        return $this->db->delete(T_DUYURULAR, "id = " .  $where);
    }


    public function getToBeEdited($table, $id, $column = "id")  {
        $array= array(
            "id" => $id
        );

        $sql = "SELECT * FROM $table WHERE $column = :id";
        return $this->db->select($sql, $array);
    }

    public function getMaxVal($table, $value)
    {
        $sql = "SELECT MAX($value) as $value FROM $table";
        return $this->db->select($sql);
    }



    public function icerikEkle($table, $data){
        return $this->db->insert($table, $data);
    }

    // Düzenlenecek
    public function ozelListele($table, $where = "", $ascOrdesc = "ASC", $order = null, $select = "*"){
        if($order == null){
            $sql = "SELECT $select FROM $table $where";
        }else{
            $sql = "SELECT $select FROM $table ORDER BY $order $ascOrdesc $where";
        }

        return $this->db->select($sql);
    }

    // Tablolarda sıralamada kullanılıyor. ozelListele'den farkı ayrı bir -birer birer artan şekilde-  newRank değişkeni oluşturması ve buna göre işlem yapması.
    public function upDownFunk($table, $where = ""){

        //$sql = "SELECT * FROM $table $where";

        $sql    =   "SELECT id, newRank, rank
                    FROM (

            SELECT id, rank, @curRank := @curRank +1 AS newRank
                    FROM $table p, (

        SELECT @curRank :=0
                    )r
                    ORDER BY rank
                    )T
                    $where";

        return $this->db->select($sql);
    }

    public function icerikSil($table, $where){

        $array= array(
            ":id" => $where
        );

        return $this->db->delete($table, "id = " .  $where);
    }

    public function tuzGetir($array = array()){
        $sql = "SELECT * FROM ". T_KULLANICILAR ." WHERE k_adi = :k_adi";
        $count = $this->db->affectedRows($sql, $array);

        if($count > 0){
            //Kullanıcı adı şifre uyuşuyor.
            $sql = "SELECT * FROM ". T_KULLANICILAR ." WHERE k_adi = :k_adi";
            return $this->db->select($sql, $array);
        }else{
            return false;
        }
    }



}
