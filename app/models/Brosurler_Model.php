<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 14.6.2015
 * Time: 13:15
 */

class Brosurler_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function brosurListGetir($limit, $sayfaSayi){
        $sql =  "SELECT ". T_BROSURLER .".*, group_concat(". T_BROSURLER_IMG .".url SEPARATOR ' & ') AS urls
                FROM ". T_BROSURLER ."
                JOIN ". T_BROSURLER_IMG ." ON ". T_BROSURLER .".id = ". T_BROSURLER_IMG .".ins_id
                GROUP BY ". T_BROSURLER .".id ORDER BY ilk_tarih DESC LIMIT $limit,$sayfaSayi";

        return $this->db->select($sql);
    }

    public function brosurSayisiGetir(){
        $sql = "SELECT COUNT(*) AS toplam FROM ". T_BROSURLER ." ORDER BY ilk_tarih ASC";
        return $this->db->select($sql);
    }

    public function menuGetir(){
        $sql = "SELECT * FROM ". T_SAYFALAR ." ORDER BY rank ASC";
        return $this->db->select($sql);
    }
} 