<?php

class Index_Model extends Model{
    public function __construct() {
        parent::__construct();
    }

    public function menuGetir(){
        $sql = "SELECT * FROM ". T_SAYFALAR ." ORDER BY rank ASC";
        return $this->db->select($sql);
    }

    public function sliderListele(){
        $sql = "SELECT * FROM ". T_KAYANYAZI ." ORDER BY rank ASC";
        return $this->db->select($sql);
    }

    public function kampanyaListele(){
        $sql = "SELECT * FROM ". T_KAMPANYALAR ." WHERE(CURDATE() BETWEEN ilk_tarih AND son_tarih) ORDER BY rank ASC LIMIT 0,10";
        return $this->db->select($sql);
    }

    public function duyuruListGetir(){
        $sql = "SELECT ". T_DUYURULAR .".id, title, short_content, content, published, date, url FROM ". T_DUYURULAR ."
        INNER JOIN ". T_DUYURULAR_IMG ."  ON ". T_DUYURULAR .".id = ". T_DUYURULAR_IMG .".ann_id AND published = 1 AND is_default_pic = 1 GROUP BY ". T_DUYURULAR .".id ORDER BY date DESC LIMIT 0,5";

        return $this->db->select($sql);
    }

}
