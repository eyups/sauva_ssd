<?php header("HTTP/1.0 404 Not Found"); ?>
<!DOCTYPE html>
<html lang="tr">
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>Market Alışveriş Merkezleri</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" type="image/png" href="<?php echo SITE_PUBLIC; ?>/images/favicon.png"/>

    <!-- Bootstrap Styles -->
    <link href="<?php echo SITE_PUBLIC; ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo SITE_PUBLIC; ?>/style.css" rel="stylesheet">

    <!-- Carousel Slider -->
    <link href="<?php echo SITE_PUBLIC; ?>/css/owl-carousel.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="<?php echo SITE_PUBLIC; ?>/css/animate.min.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,300italic,700,700italic,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>

    <!-- SLIDER ROYAL CSS SETTINGS -->
    <link href="<?php echo SITE_PUBLIC; ?>/royalslider/royalslider.css" rel="stylesheet">
    <link href="<?php echo SITE_PUBLIC; ?>/royalslider/skins/default-inverted/rs-default-inverted.css" rel="stylesheet">

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?php echo SITE_PUBLIC; ?>/rs-plugin/css/settings.css" media="screen" />

    <!-- Support for HTML5 -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Enable media queries on older bgeneral_rowsers -->
    <!--[if lt IE 9]>
    <script src="<?php echo SITE_PUBLIC; ?>/js/respond.min.js"></script>  <![endif]-->

</head>

<body>


<section class="white-wrapper padding-top margin-top">
    <div class="container">
        <div class="not_found text-center">
            <img src="<?php echo SITE_PUBLIC; ?>/images/404.png" width="50%" height="50%"/><br><br>
            <p class="lead">Birşeyler yanlış gitti! Aradığınız sayfa artık bu adreste değil. <br> Lütfen <a href="<?php echo SITE_URL; ?>/Index"> Anasayfa</a>'ya gidin</p>
            <div class="widget padding-top">
            </div>
        </div>
    </div>
</section>

<!-- Main Scripts-->
<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/menu.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.simple-text-rotator.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/wow.min.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/custom.js"></script>

<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.isotope.min.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/custom-portfolio-masonry.js"></script>


</body>
</html>

<?php die(); ?>