<footer id="footer-style-2">
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="widget text-center">
                <img class="padding-top" src="<?php echo SITE_PUBLIC; ?>/images/flogo.png" alt="Alt Logo">
                <p>Market Alışveriş Merkezleri Ticaret A.Ş.</p>
                <p>Adres <br/> -- / --</p>
                <div class="social-icons text-center">
                    <span><a data-toggle="tooltip" data-placement="bottom-mid" title="Facebook" href="#"><i class="fa fa-facebook"></i></a></span>
                    <span><a data-toggle="tooltip" data-placement="bottom-mid" title="Twitter" href="#"><i class="fa fa-twitter"></i></a></span>
                </div><!-- end social icons -->
            </div><!-- end widget -->
        </div><!-- end columns -->
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="widget text-center">
                <div class="title">
                    <h3>Facebook'ta Takip Edin</h3>
                </div><!-- end title -->

            </div><!-- end widget -->
        </div><!-- end columns -->
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="widget text-center">
                <div class="title">
                    <h3>Görüşleriniz bizim için değerlidir</h3>
                </div><!-- end title -->
            </div><!-- end widget -->
        </div><!-- end columns -->
    </div><!-- end container -->
</footer><!-- end #footer_style-1 -->

<div id="copyrights">
    <div class="container">
        <div class="col-lg-5 col-md-6 col-sm-12">
            <div class="copyright-text">
                <p><?php echo $footer_text ?></p>
            </div><!-- end copyright-text -->
        </div><!-- end widget -->
        <div class="col-lg-7 col-md-6 col-sm-12 clearfix">
            <div class="footer-menu">
                <ul class="menu">
                    <?php
                    foreach($menuListe as $key => $sayfa){
                        // Kurumsal'ın yazılmasını istemiyoruz
                        if($sayfa["name"] != "Kurumsal"){
                            if($sayfa["ust_id"] == 0){
                                if($sayfa["direk_link"] == null)
                                    echo '<li><a href="' . SITE_URL . '/Sayfa/' . $sayfa['short_name'] . '">' . $sayfa['name'] . "</a></li>";
                                else
                                    echo '<li><a href="' . SITE_URL . '/'. $sayfa["direk_link"] .  '">' . $sayfa['name'] . "</a></li>";
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
        </div><!-- end large-7 -->
    </div><!-- end container -->
</div><!-- end copyrights -->

<div class="dmtop">En Üste Git</div>

<!-- Google Analytics -->
<script async>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-65856351-2', 'auto');
    ga('send', 'pageview');

</script>

<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/menu.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/jquery.simple-text-rotator.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/wow.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_PUBLIC; ?>/js/flavr.min.js" type="text/javascript"></script>

<script src='//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js'></script>
<script type="text/javascript" src="<?php echo SITE_PUBLIC; ?>/js/custom.js"></script>
<script type="text/javascript" src="<?php echo SITE_PUBLIC; ?>/js/jquery.isotope.min.js"></script>

<?php
if($sayfaBilgisi == "FotografKatilim") {
    echo "
    <script src='//www.google.com/recaptcha/api.js'></script>
    <script type=\"text/javascript\" src=\"" . SITE_PUBLIC . "/js/eyupFotograf.js?v=1.0.0.0038\"></script>
    <script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/jquery.maskedinput.js?v=1.0.0.0001\" ></script>
    <script async>
    jQuery(function($){
        $(\"#cepTel\").mask(\"(999) 999-9999\");
    });
    </script>
    <script async>
        function toUpper(obj) {
            var mystring = obj.value;
            var sp = mystring.split(' ');
            var wl=0;
            var f ,r;
            var word = new Array();
            for (i = 0 ; i < sp.length ; i ++ ) {
                f = sp[i].substring(0,1).toUpperCase();
                r = sp[i].substring(1);
                word[i] = f+r;
            }
            newstring = word.join(' ');
            obj.value = newstring;
            return true;
        }
    </script>";
    }
?>

<?php
if($sayfaBilgisi == "KiralikYer") {
    echo "
    <script src='//www.google.com/recaptcha/api.js'></script>
    <script type=\"text/javascript\" src=\"" . SITE_PUBLIC . "/js/eyupKiralik.js?v=1.0.0.0001\"></script>
    <script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/jquery.maskedinput.js?v=1.0.0.0001\" ></script>
    <script async>
    jQuery(function($){
        $(\"#cepTel\").mask(\"(999) 999-9999\");
    });
    </script>
    <script async>
        function toUpper(obj) {
            var mystring = obj.value;
            var sp = mystring.split(' ');
            var wl=0;
            var f ,r;
            var word = new Array();
            for (i = 0 ; i < sp.length ; i ++ ) {
                f = sp[i].substring(0,1).toUpperCase();
                r = sp[i].substring(1);
                word[i] = f+r;
            }
            newstring = word.join(' ');
            obj.value = newstring;
            return true;
        }
    </script>";
    }
?>

<?php
if($sayfaBilgisi == "basvuruForm") {
    echo "
<script src='//www.google.com/recaptcha/api.js'></script>
<script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/eyupIK.js?v=1.0.0.0001\"></script>
<script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/jquery.maskedinput.js?v=1.0.0.0001\" ></script>
<script async>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#foto').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#fotograf').change(function(){
        readURL(this);
    });
    $('#askerlikDurumu').change(function() {
        if($(this).val() == \"Yapildi\")
        {
            $('#askerlikText').attr('placeholder','Askerliğinizi tamamladığınız yılı yazınız');
        }else if($(this).val() == \"Tecilli\")
        {
            $('#askerlikText').attr('placeholder','Tecil tarihini giriniz.');
        }else if($(this).val() == \"Muaf\")
        {
            $('#askerlikText').attr('placeholder','Hangi sebeple muaf olduğunuzu yazınız.');
        }
    });
    $('#isDeneyimi').change(function() {
        if($(this).val() == \"Yok\")
        {
            $('#isyeriUnvani').attr('placeholder','Çalıştığınız İşyerinin Ünvanı');
            $('#kisiUnvani').attr('placeholder', \"Göreviniz/Ünvanınız\");
            $('#isGiris').attr('placeholder','İşe Giriş Tarihi');
            $('#isCikis').attr('placeholder','İşten Çıkış Tarihi');
            $('#ayrilisNedeni').attr('placeholder','Ayrılış Nedeniniz');
            $('#enSonUcret').attr('placeholder','En Son Aldığınız Ücret');
        }else if($(this).val() == \"\")
        {
            $('#isyeriUnvani').attr('placeholder','Çalıştığınız İşyerinin Ünvanı');
            $('#kisiUnvani').attr('placeholder', \"Göreviniz/Ünvanınız\");
            $('#isGiris').attr('placeholder','İşe Giriş Tarihi');
            $('#isCikis').attr('placeholder','İşten Çıkış Tarihi');
            $('#ayrilisNedeni').attr('placeholder','Ayrılış Nedeniniz');
            $('#enSonUcret').attr('placeholder','En Son Aldığınız Ücret');
        }
        else{
            $('#isyeriUnvani').attr('placeholder','Çalıştığınız İşyerinin Ünvanı  (gerekli)');
            $('#kisiUnvani').attr('placeholder', \"Göreviniz/Ünvanınız  (gerekli)\");
            $('#isGiris').attr('placeholder','İşe Giriş Tarihi  (gerekli)');
            $('#isCikis').attr('placeholder','İşten Çıkış Tarihi  (gerekli)');
            $('#ayrilisNedeni').attr('placeholder','Ayrılış Nedeniniz  (gerekli)');
            $('#enSonUcret').attr('placeholder','En Son Aldığınız Ücret  (gerekli)');
        }
    });
</script>
<script async>
    jQuery(function($){
        $(\"#cepTel\").mask(\"(999) 999-9999\");
    });
</script>
<script async>
    jQuery(function($){
        $(\"#kimlikNo\").mask(\"999 999 999 99\");
    });
</script>
<script async>
    jQuery(function($){
        $(\"#dogumTarihi\").mask(\"99.99.9999\");
    });
</script>
<script async>
    jQuery(function($){
        $(\"#mezuniyet\").mask(\"9999\");
    });
</script>
<script async>
    jQuery(function($){
        $(\"#esCep\").mask(\"(999) 999-9999\");
    });
</script>
<script async>
    jQuery(function($){
        $(\"#evTel\").mask(\"(999) 999-9999\");
    });
</script>
<script async>
    function toUpper(obj) {
        var mystring = obj.value;
        var sp = mystring.split(' ');
        var wl=0;
        var f ,r;
        var word = new Array();
        for (i = 0 ; i < sp.length ; i ++ ) {
            f = sp[i].substring(0,1).toUpperCase();
            r = sp[i].substring(1);
            word[i] = f+r;
        }
        newstring = word.join(' ');
        obj.value = newstring;
        return true;
    }
</script>";
}
?>

<?php
if($sayfaBilgisi == "IK"){
    echo "<script async>
    // Portfolio
    (function($) {
        \"use strict\";
        var container = $('.portfolio_wrapper'),
            items = container.find('.portfolio_item'),
            portfolioLayout = 'fitRows';

        if( container.hasClass('portfolio-centered') ) {
            portfolioLayout = 'masonry';
        }

        container.isotope({
            filter: '*',
            animationEngine: 'best-available',
            layoutMode: portfolioLayout,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            },
            masonry: {
            }
        }, refreshWaypoints());

        function refreshWaypoints() {
            setTimeout(function() {
            }, 1000);
        }

        $('nav.portfolio-filter ul a').on('click', function() {
            var selector = $(this).attr('data-filter');
            container.isotope({ filter: selector }, refreshWaypoints());
            $('nav.portfolio-filter ul a').removeClass('active');
            $(this).addClass('active');
            return false;
        });

        function getColumnNumber() {
            var winWidth = $(window).width(),
                columnNumber = 1;

            if (winWidth > 1200) {
                columnNumber = 2;
            } else if (winWidth > 950) {
                columnNumber = 2;
            } else if (winWidth > 600) {
                columnNumber = 2;
            } else if (winWidth > 400) {
                columnNumber = 2;
            } else if (winWidth > 250) {
                columnNumber = 1;
            }
            return columnNumber;
        }

        function setColumns() {
            var winWidth = $(window).width(),
                columnNumber = getColumnNumber(),
                itemWidth = Math.floor(winWidth / columnNumber);

            container.find('.portfolio_item').each(function() {
                $(this).css( {
                    width : itemWidth + 'px',
                    height : '370px'

                });
            });
        }

        function setPortfolio() {
            setColumns();
            container.isotope('reLayout');
        }

        container.imagesLoaded(function () {
            setPortfolio();
        });

        $(window).on('resize', function () {
            setPortfolio();
        });
    })(jQuery);
</script>";
}
?>
<?php
if($sayfaBilgisi == "SM"){
    echo "<script async>
    // Portfolio
    (function($) {
        \"use strict\";
        var container = $('.portfolio_wrapper'),
            items = container.find('.portfolio_item'),
            portfolioLayout = 'fitRows';

        if( container.hasClass('portfolio-centered') ) {
            portfolioLayout = 'masonry';
        }

        container.isotope({
            filter: '*',
            animationEngine: 'best-available',
            layoutMode: portfolioLayout,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            },
            masonry: {
            }
        }, refreshWaypoints());

        function refreshWaypoints() {
            setTimeout(function() {
            }, 1000);
        }

        $('nav.portfolio-filter ul a').on('click', function() {
            var selector = $(this).attr('data-filter');
            container.isotope({ filter: selector }, refreshWaypoints());
            $('nav.portfolio-filter ul a').removeClass('active');
            $(this).addClass('active');
            return false;
        });

        function getColumnNumber() {
            var winWidth = $(window).width(),
                columnNumber = 1;

            if (winWidth > 1200) {
                columnNumber = 2;
            } else if (winWidth > 950) {
                columnNumber = 2;
            } else if (winWidth > 600) {
                columnNumber = 2;
            } else if (winWidth > 400) {
                columnNumber = 2;
            } else if (winWidth > 250) {
                columnNumber = 1;
            }
            return columnNumber;
        }

        function setColumns() {
            var winWidth = $(window).width(),
                columnNumber = getColumnNumber(),
                itemWidth = Math.floor(winWidth / columnNumber);

            container.find('.portfolio_item').each(function() {
                $(this).css( {
                    width : itemWidth + 'px',
                    height : '370px'

                });
            });
        }

        function setPortfolio() {
            setColumns();
            container.isotope('reLayout');
        }

        container.imagesLoaded(function () {
            setPortfolio();
        });

        $(window).on('resize', function () {
            setPortfolio();
        });
    })(jQuery);
</script>";
}
?>

<?php
if($sayfaBilgisi == "Subelerimiz") {
    echo "<!-- Google Maps -->
    <script src=\"//maps.google.com/maps/api/js?sensor=true&libraries=geometry\"></script>
    <script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/subelerimiz.js\" ></script>

    ";
}
?>

<?php
    if($sayfaBilgisi == "BizeUlasin") {
        echo "
<script src='//www.google.com/recaptcha/api.js'></script>
<script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/eyupiletisim.js?v=1.0.0.0001\"></script>
<script type=\"text/javascript\" src=\"". SITE_PUBLIC ."/js/jquery.maskedinput.js?v=1.0.0.0001\" ></script>
<script async>
    jQuery(function($){
        $(\"#number\").mask(\"(999) 999-9999\");
    });
</script>
<script async>
    function toUpper(obj) {
        var mystring = obj.value;
        var sp = mystring.split(' ');
        var wl=0;
        var f ,r;
        var word = new Array();
        for (i = 0 ; i < sp.length ; i ++ ) {
            f = sp[i].substring(0,1).toUpperCase();
            r = sp[i].substring(1);
            word[i] = f+r;
        }
        newstring = word.join(' ');
        obj.value = newstring;
        return true;
    }
</script>
    ";
    }
?>
<?php


    echo "<script src=\"". SITE_PUBLIC . "/js/jquery.lighter.js\" type=\"text/javascript\"></script>";


?>

<?php
if($sayfaBilgisi == "Index"){
    // Smooth Scrolling için fakat accordionla çakışıyor Brosurler sayfasında olmaması gerek
    echo    "
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type=\"text/javascript\" src=\"". SITE_PUBLIC . "/rs-plugin/js/jquery.themepunch.plugins.min.js\"></script>
<script type=\"text/javascript\" src=\"". SITE_PUBLIC . "/rs-plugin/js/jquery.themepunch.revolution.min.js\"></script>
<script type=\"text/javascript\">
    var revapi;
    jQuery(document).ready(function() {
        revapi = jQuery('.tp-banner').revolution(
            {
                delay:9000,
                startwidth:1600,
                startheight:540,
                hideThumbs:10,
                fullWidth:\"on\",
                forceFullWidth:\"on\"
            });
    });
</script>
                <script async>
                    $(function() {
                        $('a[href*=#]:not([href=#])').click(function() {
                            if (location.pathname.replace(/^\\//,'') == this.pathname.replace(/^\\//,'') && location.hostname == this.hostname) {
                                var target = $(this.hash);
                                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                                if (target.length) {
                                    $('html,body').animate({
                                        scrollTop: target.offset().top
                                    }, 1000);
                                    return false;
                                }
                            }
                        });
                    });
                </script>";
}
?>
<?php

if($sayfaBilgisi == "Duyuru"){
    echo    "<script src='". SITE_PUBLIC ."/js/jquery.flexslider.js'></script>
<script type='text/javascript'>
(function($) {
    'use strict';
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: 'slideInLeft',
            controlNav: false,
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
})(jQuery);
</script>";
}
?>


<!-- Carousel Slider -->
<link href="<?php echo SITE_PUBLIC; ?>/css/owl-carousel.css" rel="stylesheet">

</body>
</html>


<link href="<?php echo SITE_PUBLIC; ?>/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo SITE_PUBLIC; ?>/style.min.css" rel="stylesheet">
<link href="<?php echo SITE_PUBLIC; ?>/rs-plugin/css/settings.css" rel="stylesheet">
<link href="<?php echo SITE_PUBLIC; ?>/css/flavr_popup.css" rel="stylesheet">

<?php

if($sayfaBilgisi == "Duyuru") {
    echo "<link href=\"".SITE_PUBLIC."/css/flexslider.css\" rel=\"stylesheet\">";
}

if($sayfaBilgisi == "Brosurler") {
    echo "<link href=\"".SITE_PUBLIC."/css/jquery.lighter.css\" rel=\"stylesheet\">";
}



?>

<!-- Google Fonts -->
<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>

<link href="<?php echo SITE_PUBLIC; ?>/css/animate.min.css" rel="stylesheet">
