<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            <h2><?php echo $deneme["name"];?></h2>
            <ul class="breadcrumb pull-right">
                <li>Kurumsal</li>
                <li><?php echo $deneme["name"];?></li>
            </ul>
        </div>
    </div>
</section><!-- end post-wrapper-top -->


<section class="blog-wrapper">
    <div class="container">
        <div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <div class="blog-carousel">
                            <div class="doc">
                                <div class="widget eyupul">


                                <?php echo $deneme["content"];?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
