<div id="indirimler" style="display: block; position: relative; top: -150px; visibility: hidden"></div>
<section id="indirimlerOwl" class="white-wrapper jt-shadow">
  <div class="container">
    <div id="owl-demo">
      <?php
      foreach($kampanyaListe as $key => $value){
        echo    '<div class="item tarih text-center">
        <img src="' . SITE_PUBLIC . $value["url"] . '" alt="Kampanya Resmi'. $key .'"/>
        <p>'. date('d/m/Y', strtotime($value["ilk_tarih"])) .' - '. date('d/m/Y', strtotime($value["son_tarih"])) .' tarihlerinde geçerlidir.   </p>
        </div>';
      }
      ?>
    </div>
    <div class="messagebox">
      <h1 style="visibility: hidden;">Market Marketler Zinciri</h1>
      <h2><mark class="rotate">Zengin Çeşitler, Uygun Fiyatlar, Kaliteli Ürünler, Güleryüzlü Hizmet</mark> Market'de</h2>
    </div><!-- end messagebox -->
  </div><!-- end container -->
</section><!-- end whitewrapper -->

<section id="duyurular" class="grey-wrapper jt-shadow">
  <div class="container">
    <div class="carousel_wrapper">
      <div class="title">
        <h2>Market'den Haberler & Duyurular</h2>
      </div><!-- end title -->

      <div class="margin-top">
        <div id="owl_blog_three_line" class="owl-carousel">

          <?php


          foreach($duyuruListe as $key => $value){
            echo '<div class="blog-carousel">
            <div class="entry">
            <img src="' . SITE_PUBLIC . $value["url"] . '" alt="duyuruResmi'. $key .'" class="img-responsive">
            <div class="magnifier">
            <div class="buttons">
            <a class="st" rel="bookmark" href="' . SITE_URL . "/Duyuru/Detay/" . $value["id"] . '" title="Devamını Gör"><i class="fa fa-link"></i></a>
            </div><!-- end buttons -->
            </div><!-- end magnifier -->
            <div class="post-type">
            <i class="fa fa-picture-o"></i>
            </div><!-- end pull-right -->
            </div><!-- end entry -->
            <div class="blog-carousel-header">
            <h3><a title="" href="' . SITE_URL . "/Duyuru/Detay/" . $value["id"] . '">' . str_replace(array(".", "!"),"",mb_strtoupper($value["title"])) . '</a></h3>
            <div class="blog-carousel-meta">
            <span><i class="fa fa-calendar"></i> '. date('d/m/Y', strtotime($value["date"])) .'</span>
            </div><!-- end blog-carousel-meta -->
            </div><!-- end blog-carousel-header -->
            <div class="blog-carousel-desc">
            <p>'. $value["short_content"] .'</p>
            </div><!-- end blog-carousel-desc -->
            </div><!-- end blog-carousel -->';
          }

          ?>



        </div><!-- end owl_blog -->
      </div><!-- end padding_top -->
    </div><!-- end carousel_wrapper -->
  </div>
</section>
