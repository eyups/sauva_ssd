<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            <h2>Duyuru Detayı</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="#">Kurumsal</a></li>
                <li><a href="<?php echo SITE_URL . "/Duyuru" ?>">Duyurular</a></li>
                <li><?php echo $duyuru[0]["title"]; ?></li>
            </ul>
        </div>
    </div>
</section><!-- end post-wrapper-top -->

<section class="blog-wrapper">
    <div class="container">
        <div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <div class="blog-carousel">
                            <div class="entry">
                                <div class="flexslider">
                                    <ul class="slides">
                                        <?php
                                        foreach($duyuruImages as $key => $value){
                                            echo '<li><img src="' . SITE_PUBLIC . $value["url"] . '" alt="" class="img-responsive"></li>' . "\n";
                                        }
                                        ?>
                                    </ul><!-- end slides -->
                                </div><!-- end post-slider -->
                                <div class="post-type">
                                    <i class="fa fa-camera"></i>
                                </div><!-- end pull-right -->
                            </div><!-- end entry -->
                            <div class="blog-carousel-header">
                                <h1><?php echo $duyuru[0]["title"]; ?></h1>
                                <div class="blog-carousel-meta">
                                    <span><i class="fa fa-calendar"></i> <?php echo date('d/m/y', strtotime($duyuru[0]["date"])); ?> </span>
                                </div><!-- end blog-carousel-meta -->
                            </div><!-- end blog-carousel-header -->
                            <div class="blog-carousel-desc">
                                <?php echo $duyuru[0]["content"]; ?>

                            </div><!-- end blog-carousel-desc -->
                        </div><!-- end blog-carousel -->
                    </div><!-- end col-lg-12 -->
                </div><!-- end blog-masonry -->

                <div class="clearfix"></div>

            </div><!-- end row -->
        </div><!-- end content -->
    </div><!-- end container -->
</section><!--end white-wrapper -->