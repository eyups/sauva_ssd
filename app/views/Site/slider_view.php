<!-- slider_start -->
<div class="slider-wrapper" id="slider">

<div class="tp-banner-container">
<div class="tp-banner" >
    <ul>
        <?php
        foreach($sliderListe as $key => $value){
            echo '<td><li data-transition="'.$value["effect"].'" data-slotamount="7" data-masterspeed="1500" data-bgrepeat="no-repeat" data-bgfit="cover" data-bgposition="center center">';
            echo '<img src="'.  SITE_PUBLIC . $value["url"] . '" alt="Kayan Resim'. $key .'" /></td>';
            echo '</li>';
        }
        ?>
    </ul>

<div class="tp-bannertimer"></div>
</div>
</div>
</div><!-- end slider_wrapper -->
