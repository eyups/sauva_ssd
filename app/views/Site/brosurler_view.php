<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            <h2>Broşürler</h2>
            <ul class="breadcrumb pull-right">
                <li><a href="<?php echo SITE_URL . "/Index"; ?>">Anasayfa</a></li>
                <li>Broşürler</li>
            </ul>
        </div>
    </div>
</section><!-- end post-wrapper-top -->

<section class="blog-wrapper">
    <div class="container">
        <div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="blog-masonry">
                    <div class="col-lg-12">
                        <div class="blog-carousel">
                            <div class="doc">
                                <div class="widget">
                                    <div class="about_tabbed">
                                        <?php
                                            foreach($brosurListe as $key =>$value){
                                                $urls = explode(" & ", $value["urls"]);
                                                echo    '<div class="panel-group" id="accordion2">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading active">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse' . $key . '">'. date('d.m.Y', strtotime($value["ilk_tarih"])) . " - " . date('d.m.Y', strtotime($value["son_tarih"])) .'</a>
                                                                </h4>
                                                                </div><!-- end panel-heading -->';
                                                if($key == 0)
                                                {
                                                    echo    '<div id="collapse' . $key . '" class="panel-collapse collapse in">';
                                                }
                                                else{
                                                    echo    '<div id="collapse' . $key . '" class="panel-collapse collapse">';
                                                }


                                                echo    '<div class="panel-body">';

                                                foreach($urls as $url){
                                                    $thumbURL   =   str_replace("/brosurResimler/", SITE_URL . "/Thumbnails/brosurThumb/", $url);
                                                    $url        =   str_replace("/brosurResimler/", SITE_PUBLIC . "/brosurResimler/", $url);
                                                    echo    '<div id="content" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                                    <a href="'. $url .'" data-lighter>
                                                                        <img id="deneme"  src="' . $thumbURL . '" data-magnify-src="' . $url . '">
                                                                    </a>
                                                                </div>';
                                                }
                                                echo '</div><!-- end panel-body -->
                                                </div><!-- end collapseOne -->
                                            </div><!-- end panel -->
                                        </div><!-- end panel-group -->';

                                            }
                                        ?>
                                    </div><!-- end about tabbed -->
                                </div><!-- end widget -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        <div class="clearfix"></div>

                        <hr>

                        <div class="pagination_wrapper text-center">
                            <!-- Pagination Normal -->
                            <ul class="pagination">
                                <?php
                                if($sayfaNo != 1)
                                    echo '<li><a href="' . SITE_URL . "/Brosurler/" . (intval($sayfaNo) - 1) . '">«</a></li>';
                                else
                                    echo '<li><a href="#">«</a></li>';
                                for($i = 1; $i <= $toplamSayfa; $i++) {
                                    if($sayfaNo == $i) { // Eğer Bulunduğumuz Sayfada ise farklı kod ve link yok
                                        echo '<li class="active"><a href="#">' . $i . '</a></li>';
                                    } else {
                                        echo '<li><a href="' . SITE_URL . "/Brosurler/" . $i . '">' . $i . '</a></li>';
                                    }
                                }
                                if($sayfaNo == $toplamSayfa)
                                    echo '<li><a href="#">»</a></li>';
                                else
                                    echo '<li><a href="' . SITE_URL . "/Brosurler/" . (intval($sayfaNo) + 1) . '">»</a></li>';

                                ?>
                            </ul>
                        </div><!-- end pagination_wrapper -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
</section><!-- end transparent-bg -->

                    <div class="clearfix"></div>