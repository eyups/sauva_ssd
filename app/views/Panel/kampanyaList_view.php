<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Kampanyalar</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <?php
    if(isset($remResult[0])) {
        if ($remResult[0]) {
            echo '<div class="alert alert-success">';
            echo '<p>İşleminiz başarıyla gerçekleştirildi.</p>';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger">';
            echo '<p>HATA : İşleminiz sırasında hata oluştu. Lütfen yeniden deneyiniz.</p>';
            echo '</div>';
        }
    }

    ?>


    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Kampanya Listesi
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>İsim</th>
                        <th>URL</th>
                        <th>Başlama Tarihi</th>
                        <th>Bitiş Tarihi</th>
                        <th>Önizleme</th>
                        <th>Düzenle</th>
                        <th>Sil</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($kampanyaListe as $key => $value){
                        echo '<tr class="odd gradeX">' . "\n";
                        echo '<td>'. $value["id"] . '</td>' . "\n";
                        echo '<td>'. $value["name"] . '</td>'. "\n";
                        echo '<td><a target="_blank" href="' . SITE_PUBLIC . $value["url"] . '"/>' . $value["url"]. '</a>' . '</td>'. "\n";
                        echo '<td>'. $value["ilk_tarih"] . '</td>'. "\n";
                        echo '<td>'. $value["son_tarih"] . '</td>'. "\n";
                        $value["url"] = str_replace("/kampanyaResimler/", "", $value["url"]);
                        echo '<td><img src="' . SITE_URL . "/Thumbnails/kampanyaThumb/" . $value["url"]. '"/></td>'. "\n";
                        echo '<td class="center"><a href="'.SITE_URL.'/Panel/kampanyaEdit/'. $value["id"]. '" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span></a></td>'. "\n";
                        echo '<td class="center"><a href="#" id="'.SITE_URL.'/Panel/kampanyaRemove/'. $value["id"]. '" onclick="clicked(this);" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a></td>'. "\n";
                        echo '</tr>' . "\n";
                        //
                    }
                    ?>

                    </tbody>
                    </table>


                    </div>

                </div>
            </div>
            </div>
    </div>
</div>
