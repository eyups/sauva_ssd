<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Duyurular</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
        if(isset($formErrors)){
            foreach($formErrors as $key => $value){
                switch($key){
                    case 'title':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BAŞLIK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'short_content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "ÖN YAZI: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "İÇERİK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'resim':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "RESİM: " . $val;
                            echo "</div>";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Duyuru Düzenle - <?php print_r($toBeEdited[0]["title"]); ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/doDuyuruEdit/<?php print_r($toBeEdited[0]["id"]); ?>"  method="POST">
                            <div class="form-group">
                                <label for="title">Başlık (gerekli)</label>
                                <input type="text" name="title" class="form-control"  value="<?php print_r($toBeEdited[0]["title"]); ?>">
                            </div>
                            <div class="form-group">
                                <label for="short_content">Ön Yazı (gerekli)</label>
                                <textarea name="short_content" class="form-control" rows="4"><?php print_r($toBeEdited[0]["short_content"]); ?></textarea>
                            </div>
                            <div class="form-group right">
                                <label for="content">İçerik (gerekli)</label>
                                <textarea name="content" class="ckeditor" rows="10"><?php print_r($toBeEdited[0]["content"]); ?></textarea>
                            </div>
                            <div class="form-group">
                                <div class="image-container">
                                    <ul class="list-inline">
                                        <?php
                                        if(isset($duyuruImages)){
                                            foreach($duyuruImages as $key => $value){
                                                $value["url"] = str_replace("/duyuruResimler/", "", $value["url"]);
                                                echo '<li>';
                                                echo "<div style=\"width: 160px; height:78px; background-image: url('" . SITE_URL . '/Thumbnails/duyuruThumb/' . $value["url"] . "'); background-size:   cover; background-repeat: no-repeat; \">";
                                                //echo '<img src="' . SITE_URL . '/Thumbnails/duyuruThumb/' . $value["url"] . '" />';
                                                echo '<a href="#" title="Sil" onclick="clicked(this);" id="' . SITE_URL . '/Panel/removeDuyuruImage/' . $value["id"] . '" class="btn btn-default remove-button"><span class="fa fa-times-circle" style="color: red;"></span></a>';
                                                if(intval($value["is_default_pic"]) == 0)
                                                    echo '<a href="#" title="Varsayılan resim yap" onclick="varsayilan(this);" id="' . SITE_URL . '/Panel/varsayilanYap/' . $value["id"] . '" class="btn btn-default remove-button"><span class="fa fa-check-circle-o" style="color: green;"></span></a>';
                                                echo '</div>';
                                                echo '</li>';
                                            }
                                            echo    '<br/>';
                                            echo    '<label for="resim[]">Resimleri Yükle</label> <span style="color: red;">Yeni Resim Yükle</span>
                                                    <input type="file" id="resim" name="resim[]" multiple="multiple" accept="image/*" />';
                                        }else{
                                            echo    '<label for="resim[]">Resimleri Yükle</label> <span style="color: red;">Hiç resim yüklenmemiş</span>
                                                    <input type="file" id="resim" name="resim[]" multiple="multiple" accept="image/*" />';
                                        }

                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Duyuru yayımlansın mı ?</label>

                                <?php
                                if($toBeEdited[0]["published"] == 1)
                                    echo    '<label class="radio-inline">
                                                <input type="radio" name="published" id="optionsRadiosInline1" value="1" checked>Evet
                                            </label>
                                            <label class="radio-inline">
                                                 <input type="radio" name="published" id="optionsRadiosInline2" value="0">Hayır, beklet
                                            </label>';
                                else
                                    echo    '<label class="radio-inline">
                                                 <input type="radio" name="published" id="optionsRadiosInline1" value="1">Evet
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="published" id="optionsRadiosInline2" value="0" checked>Hayır, beklet
                                            </label>';
                                ?>


                            </div>

                            <input type="submit" name="gonder" class="btn btn-default">
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
