<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Duyurular</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <?php
    if(isset($remResult[0])) {
        if ($remResult[0]) {
            echo '<div class="alert alert-success">';
            echo '<p>İşleminiz başarıyla gerçekleştirildi.</p>';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger">';
            echo '<p>HATA : İşleminiz sırasında hata oluştu. Lütfen yeniden deneyiniz.</p>';
            echo '</div>';
        }
    }

    ?>


    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Duyuru Listesi
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Başlık</th>
                        <th>Ön Yazı</th>
                        <th>Durum</th>
                        <th>Tarih</th>
                        <th>Düzenle</th>
                        <th>Sil</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($duyuruListe as $key => $value){
                        echo '<tr class="odd gradeX">' . "\n";
                        echo '<td>'. $value["id"] . '</td>' . "\n";
                        echo '<td>'. $value["title"] . '</td>' . "\n";
                        echo '<td>'. $value["short_content"] . '</td>'. "\n";
                        echo '<td>'. $value["published"] . '</td>'. "\n";
                        echo '<td>'. $value["date"] . '</td>'. "\n";
                        echo '<td class="center"><a href="'.SITE_URL.'/Panel/duyuruEdit/'. $value["id"]. '" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span></a></td>'. "\n";
                        echo '<td class="center"><a id="'.SITE_URL.'/Panel/duyuruRemove/'. $value["id"]. '" onclick="clicked(this);" href="#" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a></td>'. "\n";
                        echo '</tr>' . "\n";
                    }
                    ?>

                    </tbody>
                    </table>
                    </div>
                </div>
            </div>
            </div>
    </div>
</div>
