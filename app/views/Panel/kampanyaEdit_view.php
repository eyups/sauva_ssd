<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Kampanyalar</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
            if(isset($formErrors)){
                foreach($formErrors as $key => $value){
                    switch($key){
                        case 'name':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "İSİM: " . $val;
                                echo "</div>";
                            }
                            break;
                        case 'resim':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "RESİM: " . $val;
                                echo "</div>";
                            }
                            break;
                        case 'ilk_tarih':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "İLK TARİH: " . $val;
                                echo "</div>";
                            }
                            break;
                        case 'son_tarih':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "SON TARİH: " . $val;
                                echo "</div>";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

        ?>



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Kampanya Düzenle
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/doKampanyaEdit/<?php print_r($toBeEdited[0]["id"]); ?>"  method="POST">
                            <div class="form-group">
                                <label for="title">İsim (gerekli)</label>
                                <input type="text" name="name" class="form-control"  value="<?php print_r($toBeEdited[0]["name"]); ?>"">
                            </div>
                            <div class="form-group">
                                <label>Başlangıç Tarihi</label><br/>
                                <input type="date" name="ilk_tarih" value="<?php print_r($toBeEdited[0]["ilk_tarih"]); ?>"">
                            </div>
                            <div class="form-group">
                                <label>Bitiş Tarihi</label><br/>
                                <input type="date" name="son_tarih" value="<?php print_r($toBeEdited[0]["son_tarih"]); ?>"">
                            </div>
                            <div class="form-group">
                                <label>Önizleme:</label><br />
                                <img src="<?php
                                $toBeEdited[0]["url"]   =   str_replace("/kampanyaResimler/", "" , $toBeEdited[0]["url"]);
                                echo SITE_URL . "/Thumbnails/kampanyaThumb/" . $toBeEdited[0]["url"];
                                ?>"/>
                            </div>
                            <input type="hidden" name="MAX_FILE_SIZE" value="25000" />
                            <button type="submit" name="gonder" class="btn btn-default">Düzenle</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
