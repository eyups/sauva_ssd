<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Yönetici Hesabı</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
        if(isset($formErrors)){
            foreach($formErrors as $key => $value){
                switch($key){
                    case 'title':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BAŞLIK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'short_content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "ÖN YAZI: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "İÇERİK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'resim':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "RESİM: " . $val;
                            echo "</div>";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ?>

    <?php
    if(isset($remResult[0])) {
        if ($remResult[0]) {
            echo '<div class="alert alert-success">';
            echo '<p>İşleminiz başarıyla gerçekleştirildi. Sistemden otomatik olarak çıkış yaptınız. Bir sonraki sayfada giriş bilgileriniz tekrar istenecektir.</p>';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger">';
            echo '<p>HATA : Şifre değiştirme işlemi gerçekleştirilirken hata oluştu.</p>';
            echo '</div>';
        }
    }

    ?>

    <?php
    if(isset($isSame)) {
        if (!$isSame) {
            echo '<div class="alert alert-danger">';
            echo '<p>HATA : Şifreleriniz birbiriyle eşleşmiyor.</p>';
            echo '</div>';
        }
    }

    ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Şifre Değiştir
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/changeSifre"  method="POST">
                            <div class="form-group">
                                <label for="title">Yeni Şifre</label>
                                <input type="password" name="yeniSifre" class="form-control"  autofocus="true">
                            </div>
                            <div class="form-group">
                                <label for="title">Yeni Şifre (Tekrar)</label>
                                <input type="password" name="yeniSifreT" class="form-control"  autofocus="true">
                            </div>
                            <button type="submit" name="gonder" class="btn btn-default">Değiştir</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
