<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Anasayfa Kayan Resim</h1>
        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->
    <?php
    if(isset($formErrors)){
        foreach($formErrors as $key => $value){
            switch($key){
                case 'name':
                    foreach($value as $val){
                        echo "<div class='alert alert-danger'>";
                        echo "İSİM: " . $val;
                        echo "</div>";
                    }
                    break;
                case 'resim':
                    foreach($value as $val){
                        echo "<div class='alert alert-danger'>";
                        echo "RESİM: " . $val;
                        echo "</div>";
                    }
                    break;
                default:
                    break;
            }
        }
    }

    ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Anasayfa Kayan Resim
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-9">

                            <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/doAnasayfaMedyaEdit/<?php print_r($toBeEdited[0]["id"]); ?>"  method="POST">
                                <div class="form-group">
                                    <label for="title">İsim (gerekli)</label>
                                    <input type="text" name="name" class="form-control" value="<?php print_r($toBeEdited[0]["name"]); ?>">
                                </div>
                                <div class="form-group">
                                    <label>Geçiş Efekti</label>
                                    <select name="effect" class="form-control">
                                        <option <?php if ($toBeEdited[0]["effect"] == 'fade') echo ' selected="selected"'; ?>>fade</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'papercut') echo ' selected="selected"'; ?>>papercut</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'curtain-1') echo ' selected="selected"'; ?>>curtain-1</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'curtain-2') echo ' selected="selected"'; ?>>curtain-2</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'curtain-3') echo ' selected="selected"'; ?>>curtain-3</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == '3dcurtain-horizontal') echo ' selected="selected"'; ?>>3dcurtain-horizontal</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == '3dcurtain-vertical') echo ' selected="selected"'; ?>>3dcurtain-vertical</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'random') echo ' selected="selected"'; ?>>random</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'turnoff') echo ' selected="selected"'; ?>>turnoff</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'turnoff-vertical') echo ' selected="selected"'; ?>>turnoff-vertical</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'flyin') echo ' selected="selected"'; ?>>flyin</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'parallaxtoright') echo ' selected="selected"'; ?>>parallaxtoright</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'parallaxtoleft') echo ' selected="selected"'; ?>>parallaxtoleft</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'parallaxtotop') echo ' selected="selected"'; ?>>parallaxtotop</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'parallaxtobottom') echo ' selected="selected"'; ?>>parallaxtobottom</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'slideright') echo ' selected="selected"'; ?>>slideright</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'slideleft') echo ' selected="selected"'; ?>>slideleft</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'slideup') echo ' selected="selected"'; ?>>slideup</option>
                                        <option <?php if ($toBeEdited[0]["effect"] == 'slidedown') echo ' selected="selected"'; ?>>slidedown</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Önizleme:</label><br />
                                    <img src="<?php
                                    $toBeEdited[0]["url"]   =   str_replace("/medyaResimler/", "" , $toBeEdited[0]["url"]);
                                        echo SITE_URL . "/Thumbnails/createThumb/" . $toBeEdited[0]["url"];
                                    ?>"/> <br />
                                </div>
                                <button type="submit" name="gonder" class="btn btn-default">Düzenle</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
