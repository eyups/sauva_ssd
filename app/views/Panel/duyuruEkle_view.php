<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Duyurular</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
        if(isset($formErrors)){
            foreach($formErrors as $key => $value){
                switch($key){
                    case 'title':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BAŞLIK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'short_content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "ÖN YAZI: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "İÇERİK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'resim':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "RESİM: " . $val;
                            echo "</div>";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Duyuru Ekle
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">
                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/doDuyuruEkle"  method="POST">
                            <div class="form-group">
                                <label for="title">Başlık (gerekli)</label>
                                <input type="text" name="title" class="form-control"  autofocus="true" value="<?php if(isset($beforeErr)) if(!empty(print_r($beforeErr["title"]))); ?>">
                            </div>
                            <div class="form-group">
                                <label for="short_content">Ön Yazı (gerekli)</label>
                                <textarea name="short_content" class="form-control" rows="4"><?php if(isset($beforeErr)) if(!empty(print_r($beforeErr["short_content"]))); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="content">İçerik (gerekli)</label>
                                <textarea name="content" class="ckeditor" rows="10"><?php if(isset($beforeErr)) if(!empty(print_r($beforeErr["content"]))); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="resim[]">Resimleri Yükle</label> <?php if(isset($beforeErr) && isset($beforeErr["resim"])) if(!empty(print_r($beforeErr["resim"]))); ?>
                                <input type="file" id="resim" name="resim[]" multiple="multiple" accept="image/*" />
                            </div>
                            <div class="form-group">
                                <label>Duyuru yayımlansın mı ?</label>
                                <label class="radio-inline">
                                    <input type="radio" name="published" id="optionsRadiosInline1" value="1" checked>Evet
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="published" id="optionsRadiosInline2" value="0">Hayır, beklet
                                </label>
                            </div>

                            <button type="submit" name="gonder" class="btn btn-default">Ekle</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
