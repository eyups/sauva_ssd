<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Sayfalar</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
            if(isset($formErrors)){
                foreach($formErrors as $key => $value){
                    switch($key){
                        case 'name':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "İSİM: " . $val;
                                echo "</div>";
                            }
                            break;
                        case 'content':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "İÇERİK: " . $val;
                                echo "</div>";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

        ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sayfa Ekle
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form action="<?php echo SITE_URL; ?>/Panel/doSayfaEkle"  method="POST">
                            <div class="form-group">
                                <label for="title">İsim (gerekli)</label>
                                <input type="text" name="name" class="form-control"  autofocus="true">
                            </div>
                            <div class="form-group">
                                <label for="title">İçerik (gerekli)</label>
                                <textarea name="content" class="ckeditor" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Üst Sayfa</label>
                                <select name="ust_id" class="form-control">
                                    <option value="">Yok</option>
                                    <?php
                                    foreach($sayfaListe as $key => $value) {
                                        echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>' . "\n";

                                    }

                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ana Menü'de Görünsün mü ?</label>
                                <label class="radio-inline">
                                    <input type="radio" name="anamenu" id="optionsRadiosInline1" value="1" checked>Evet
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="anamenu" id="optionsRadiosInline2" value="0">Hayır
                                </label>
                            </div>

                            <button type="submit" name="gonder" class="btn btn-default">Ekle</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
