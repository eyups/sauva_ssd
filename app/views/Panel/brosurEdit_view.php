<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Broşürler</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
        if(isset($formErrors)){
            foreach($formErrors as $key => $value){
                switch($key){
                    case 'title':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BAŞLIK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'short_content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "ÖN YAZI: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'content':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "İÇERİK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'resim':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "RESİM: " . $val;
                            echo "</div>";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Broşür Düzenle - <?php print_r($toBeEdited[0]["title"]); ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/doBrosurEdit/<?php print_r($toBeEdited[0]["id"]); ?>"  method="POST">
                            <div class="form-group">
                                <label for="title">Başlık (gerekli)</label>
                                <input type="text" name="title" class="form-control"  value="<?php print_r($toBeEdited[0]["title"]); ?>">
                            </div>
                            <div class="form-group">
                                <div class="image-container">
                                    <ul class="list-inline">
                                        <?php
                                        if(isset($brosurImages)){
                                            foreach($brosurImages as $key => $value){
                                                $value["url"] = str_replace("/brosurResimler/", "", $value["url"]);
                                                echo '<li>';
                                                echo '<img src="' . SITE_URL . '/Thumbnails/brosurThumb/' . $value["url"] . '" />';
                                                echo '<a href="#" onclick="clicked(this);" id="' . SITE_URL . '/Panel/removeBrosurImage/' . $value["id"] . '" class="btn btn-default remove-button"><span class="fa fa-times-circle" style="color: red;"></span></a>';
                                                echo '</li>';
                                            }
                                            echo    '<br/>';
                                            echo    '<label for="resim[]">Resimleri Yükle</label> <span style="color: red;">Yeni Resim Yükle</span>
                                                    <input type="file" id="resim" name="resim[]" multiple="multiple" accept="image/*" />';
                                        }else{
                                            echo    '<label for="resim[]">Resimleri Yükle</label> <span style="color: red;">Hiç resim yüklenmemiş</span>
                                                    <input type="file" id="resim" name="resim[]" multiple="multiple" accept="image/*" />';
                                        }

                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Başlangıç Tarihi</label><br/>
                                <input type="date" name="ilk_tarih" value="<?php print_r($toBeEdited[0]["ilk_tarih"]); ?>"">
                            </div>
                            <div class="form-group">
                                <label>Bitiş Tarihi</label><br/>
                                <input type="date" name="son_tarih" value="<?php print_r($toBeEdited[0]["son_tarih"]); ?>"">
                            </div>

                            <input type="submit" name="gonder" class="btn btn-default">
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
