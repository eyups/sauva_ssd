


<!-- jQuery -->
<script src="<?php echo SITE_PUBLIC; ?>/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo SITE_PUBLIC; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo SITE_PUBLIC; ?>/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo SITE_PUBLIC; ?>/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo SITE_PUBLIC; ?>/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo SITE_PUBLIC; ?>/dist/js/sb-admin-2.js"></script>

<!-- CK Editor JS -->
<script src="<?php echo SITE_PUBLIC; ?>/ckeditor/ckeditor.js"></script>

<!-- Flavr -->
<script type="text/javascript" src="<?php echo SITE_PUBLIC; ?>/js/flavr.js"></script>



<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    /*$(document).ready(function() {
     $('#dataTables-example').DataTable({
     responsive: true
     });
     });*/
</script>

<script type="text/javascript">
        /*  -------------------------------------------------------------------------------
         Confirm Dialog
         ------------------------------------------------------------------------------- */
        function clicked(item){

            var adres = item.id;

            new $.flavr({
                content     : 'Silmek istediğinize emin misiniz?',
                dialog      : 'confirm',
                overlay     : false,
                animateEntrance : 'tada',
                animateClosing  : 'slideOutRight',
                onConfirm   : function(){
                    location.replace(adres);
                },
                onCancel    : function(){

                }
            });

        }

        function varsayilan(item){

            var adres = item.id;

            new $.flavr({
                content     : 'Varsayılan fotoğraf yapmak istediğinizden emin misiniz?',
                dialog      : 'confirm',
                overlay     : false,
                animateEntrance : 'tada',
                animateClosing  : 'slideOutRight',
                onConfirm   : function(){
                    location.replace(adres);
                },
                onCancel    : function(){

                }
            });

        }
</script>

</body>

</html>