<div id="page-wrapper">

    <section id="auth-button"></section>
    <section id="view-selector"></section>
    <section id="timeline"></section>
    <section id="timeline4"></section>
    <section id="timeline2"></section>
    <section id="timeline3"></section>


</div>
<!-- /#wrapper -->


<script>
    (function(w,d,s,g,js,fjs){
        g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
        js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
        js.src='https://apis.google.com/js/platform.js';
        fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
    }(window,document,'script'));
</script>

<script>
    gapi.analytics.ready(function() {

        // Step 3: Authorize the user.

        var CLIENT_ID = '808592378784-ir9tcu8qkd6m0d3gc0bij2kuprbob7gd.apps.googleusercontent.com';

        gapi.analytics.auth.authorize({
            container: 'auth-button',
            clientid: CLIENT_ID,
        });

        // Step 4: Create the view selector.

        var viewSelector = new gapi.analytics.ViewSelector({
            container: 'view-selector'
        });

        // Step 5: Create the timeline chart.

        var timeline = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:sessions',
                'start-date': '30daysAgo',
                'end-date': 'today',
            },
            chart: {
                type: 'LINE',
                container: 'timeline'
            }
        });

        var timeline2 = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:pageviews',
                'start-date': '30daysAgo',
                'end-date': 'today',
            },
            chart: {
                type: 'COLUMN',
                container: 'timeline2'
            }
        });

        var timeline3 = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:city',
                'metrics': 'ga:pageviews',
                'start-date': '30daysAgo',
                'end-date': 'today',
            },
            chart: {
                type: 'COLUMN',
                container: 'timeline3'
            }
        });

        var timeline4 = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:city',
                'metrics': 'ga:sessions',
                'start-date': '30daysAgo',
                'end-date': 'today',
            },
            chart: {
                type: 'COLUMN',
                container: 'timeline4'
            }
        });

        // Step 6: Hook up the components to work together.

        gapi.analytics.auth.on('success', function(response) {
            viewSelector.execute();
        });

        viewSelector.on('change', function(ids) {
            var newIds = {
                query: {
                    ids: ids
                }
            }
            timeline.set(newIds).execute();
            timeline2.set(newIds).execute();
            timeline3.set(newIds).execute();
            timeline4.set(newIds).execute();
        });
    });
</script>

</body>

</html>
