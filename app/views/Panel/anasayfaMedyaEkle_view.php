<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Anasayfa Kayan Resim</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
            if(isset($formErrors)){
                foreach($formErrors as $key => $value){
                    switch($key){
                        case 'name':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "İSİM: " . $val;
                                echo "</div>";
                            }
                            break;
                        case 'resim':
                            foreach($value as $val){
                                echo "<div class='alert alert-danger'>";
                                echo "RESİM: " . $val;
                                echo "</div>";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

        ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Anasayfa Kayan Resim Ekle
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/uploadImage"  method="POST">
                            <div class="form-group">
                                <label for="title">İsim (gerekli)</label>
                                <input type="text" name="name" class="form-control"  autofocus="true">
                            </div>
                            <div class="form-group">
                                <label>Geçiş Efekti</label>
                                <select name="effect" class="form-control">
                                    <option>fade</option>
                                    <option>papercut</option>
                                    <option>curtain-1</option>
                                    <option>curtain-2</option>
                                    <option>curtain-3</option>
                                    <option>3dcurtain-horizontal</option>
                                    <option>3dcurtain-vertical</option>
                                    <option>random</option>
                                    <option>turnoff</option>
                                    <option>turnoff-vertical</option>
                                    <option>flyin</option>
                                    <option>parallaxtoright</option>
                                    <option>parallaxtoleft</option>
                                    <option>parallaxtotop</option>
                                    <option>parallaxtobottom</option>
                                    <option>slideright</option>
                                    <option>slideleft</option>
                                    <option>slideup</option>
                                    <option>slidedown</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Resim Yükle</label>
                                <input type="file" name="resim">
                            </div>
                            <input type="hidden" name="MAX_FILE_SIZE" value="25000" />
                            <button type="submit" name="gonder" class="btn btn-default">Ekle</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
