<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Site Ayarları</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
        if(isset($formErrors)){
            foreach($formErrors as $key => $value){
                switch($key){
                    case 'facebook':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "FACEBOOK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'twitter':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "TWITTER: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'site_title':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "SİTE BAŞLIĞI: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'email':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "EMAİL: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'phone':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "TEL NUMARA: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'footer_text':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "ALT COPYRIGHT TEXT: " . $val;
                            echo "</div>";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ?>

    <?php
    if(isset($remResult[0])) {
        if ($remResult[0]) {
            echo '<div class="alert alert-success">';
            echo '<p>İşleminiz başarıyla gerçekleştirildi.</p>';
            echo '</div>';
        } else {
            echo '<div class="alert alert-danger">';
            echo '<p>HATA : İşleminiz sırasında hata oluştu. Lütfen yeniden deneyiniz.</p>';
            echo '</div>';
        }
    }

    ?>

    <?php
    if(isset($isSame)) {
        if (!$isSame) {
            echo '<div class="alert alert-danger">';
            echo '<p>HATA : Şifreleriniz birbiriyle eşleşmiyor.</p>';
            echo '</div>';
        }
    }

    ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Site Ayarları
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/saveSettings"  method="POST">

                            <div class="form-group">
                                <label for="title">Facebook</label>
                                <input type="text" name="facebook" class="form-control"  autofocus="true" value="<?php echo $ayarListe[0]["val"]  ?>">
                            </div>
                            <div class="form-group">
                                <label for="title">Twitter</label>
                                <input type="text" name="twitter" class="form-control"  autofocus="true" value="<?php echo $ayarListe[1]["val"]  ?>">
                            </div>
                            <div class="form-group">
                                <label for="title">Site Başlığı</label>
                                <input type="text" name="site_title" class="form-control"  autofocus="true" value="<?php echo $ayarListe[2]["val"]  ?>">
                            </div>
                            <div class="form-group">
                                <label for="title">Email</label>
                                <input type="text" name="email" class="form-control"  autofocus="true" value="<?php echo $ayarListe[3]["val"]  ?>">
                            </div>
                            <div class="form-group">
                                <label for="title">Telefon</label>
                                <input type="text" name="phone" class="form-control"  autofocus="true" value="<?php echo $ayarListe[4]["val"]  ?>">
                            </div>
                            <div class="form-group">
                                <label for="title">Alt Copyright Yazısı</label>
                                <input type="text" name="footer_text" class="form-control"  autofocus="true" value="<?php echo $ayarListe[5]["val"]  ?>">
                            </div>
                            <button type="submit" name="gonder" class="btn btn-default">Ayarları Kaydet</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
