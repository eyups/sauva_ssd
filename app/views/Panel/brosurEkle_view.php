<div id="page-wrapper">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Broşürler</h1>
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.row -->
        <?php
        if(isset($formErrors)){
            foreach($formErrors as $key => $value){
                switch($key){
                    case 'title':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BAŞLIK: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'ilk_tarih':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BAŞLANGIÇ TARİHİ: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'son_tarih':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "BİTİŞ TARİHİ: " . $val;
                            echo "</div>";
                        }
                        break;
                    case 'resim':
                        foreach($value as $val){
                            echo "<div class='alert alert-danger'>";
                            echo "RESİM: " . $val;
                            echo "</div>";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Broşür Ekle
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form enctype="multipart/form-data" action="<?php echo SITE_URL; ?>/Panel/doBrosurEkle"  method="POST">
                            <div class="form-group">
                                <label for="title">Başlık (gerekli)</label>
                                <input type="text" name="title" class="form-control"  autofocus="true">
                            </div>
                            <div class="form-group">
                                <label for="resim[]">Resimleri Yükle</label>
                                <input type="file" id="resim" name="resim[]" multiple="multiple" accept="image/*" />
                            </div>
                            <div class="form-group">
                                <label>Başlangıç Tarihi</label><br/>
                                <input type="date" name="ilk_tarih">
                            </div>
                            <div class="form-group">
                                <label>Bitiş Tarihi</label><br/>
                                <input type="date" name="son_tarih">
                            </div>

                            <button type="submit" name="gonder" class="btn btn-default">Ekle</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</div>
<!-- /#wrapper -->



</body>

</html>
