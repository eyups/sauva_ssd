<?php
/**
* Created by PhpStorm.
* User: EyüpSabri
* Date: 10.6.2015
* Time: 11:31
*/

class Form {
  public $currentValue;
  public $values = array();
  public $errors = array();


  public $turler = array("image/jpeg", "image/jpg", "image/x-png", "image/png");
  public $uzantilar = array("jpeg", "jpg", "png");

  public $uzantiJPEG = array("jpeg", "jpg");
  public $turJPEG = array("image/jpeg", "image/jpg");

  public function __construct(){}

    public function post($key){
      $this->values[$key] =   trim($_POST[$key]);

      $this->currentValue =   $key;

      return $this;
    }

    public function resim($key){
      //$this->values[$key] =   trim($_FILES[$key]['tmp_name']);
      $this->currentValue =   $key;

      return $this;
    }

    public function isEmpty(){
      if(empty($this->values[$this->currentValue])){
        $this->errors[$this->currentValue]["empty"] =   "Lütfen bu alanı boş bırakmayınız.";
      }
      return $this;
    }

    public function isImage(){
      $turu = $_FILES[$this->currentValue]['type'];
      $resim = $_FILES[$this->currentValue]['name'];

      $uzanti = substr($resim, strpos($resim, ".") + 1);

      if(!in_array($turu, $this->turler) && !in_array($uzanti, $this->uzantilar)){
        $this->errors[$this->currentValue]["image"] =   "Lütfen geçerli bir resim dosyası seçiniz.";
      }
      return $this;
    }

    public function isJPEG(){
      $turu = $_FILES[$this->currentValue]['type'];
      $resim = $_FILES[$this->currentValue]['name'];

      $uzanti = substr($resim, strpos($resim, ".") + 1);

      if(!in_array($turu, $this->turJPEG) && !in_array($uzanti, $this->uzantiJPEG)){
        $this->errors[$this->currentValue]["image"] =   "Lütfen JPEG türünde geçerli bir resim dosyası seçiniz.";
      }
      return $this;
    }


    public function mul_isJPEG(){

      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {

        $turu = $_FILES[$this->currentValue]['type'][$f];
        $resim = $_FILES[$this->currentValue]['name'][$f];

        $uzanti = substr($resim, strpos($resim, ".") + 1);

        if (!in_array($turu, $this->turJPEG) && !in_array($uzanti, $this->uzantiJPEG)) {
          $this->errors[$this->currentValue]["image"] = "Lütfen JPEG türünde geçerli resim dosyaları seçiniz.";

        }
      }
      return $this;
    }

    public function mul_isImage(){

      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
        $turu = $_FILES[$this->currentValue]['type'][$f];
        $resim = $_FILES[$this->currentValue]['name'][$f];

        $uzanti = substr($resim, strpos($resim, ".") + 1);

        if (!in_array($turu, $this->turler) && !in_array($uzanti, $this->uzantilar)) {
          $this->errors[$this->currentValue]["image"] = "Lütfen geçerli resim dosyaları seçiniz.";

        }
      }
      return $this;
    }

    public function mul_checkRes($width, $height){

      // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
      $edit = array_filter($_FILES[$this->currentValue]['name']);

      if(!empty($edit)) {
        foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
          $image_info = getimagesize($_FILES[$this->currentValue]["tmp_name"][$f]);
          $image_width = $image_info[0];
          $image_height = $image_info[1];
          if ($image_width > $width || $image_height > $height) {
            $this->errors[$this->currentValue]["resolution"] = "Resim dosyası istenilen (" . $width . " x " . $height . ") çözünürlülükten küçük olmalıdır.";

          }
        }
      }

      return $this;
    }

    public function mul_checkSize($istenenBoyut = 3072){
      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
        $boyut = $_FILES[$this->currentValue]['size'][$f];

        if ($boyut > $istenenBoyut * 1024) {
          $this->errors[$this->currentValue]["size"] = "Dosya boyutu " . $istenenBoyut ." KB'dan az olmalıdır.";
        }
      }
      return $this;
    }

    public function mul_checkMinSize($istenenBoyut = 1024){
      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
        $boyut = $_FILES[$this->currentValue]['size'][$f];

        if ($boyut > $istenenBoyut * 1024) {
          $this->errors[$this->currentValue]["size"] = "Dosya boyutu " . $istenenBoyut ." KB'dan fazla olmalıdır.";
        }
      }
      return $this;
    }

    public function mul_checkMinRes($width, $height){

      // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
      $edit = array_filter($_FILES[$this->currentValue]['name']);

      if(!empty($edit)) {
        foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
          $image_info = getimagesize($_FILES[$this->currentValue]["tmp_name"][$f]);
          $image_width = $image_info[0];
          $image_height = $image_info[1];

          if ($image_width < $width || $image_height < $height) {
            $this->errors[$this->currentValue]["resolution"] = "Resim dosyası istenilen (" . $width . " x " . $height . ") çözünürlülükten büyük olmalıdır.";

          }
        }
      }

      return $this;
    }

    public function mul_isJPEG_new(){

      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {

        $turu = $_FILES[$this->currentValue]['type'][$f];
        $resim = $_FILES[$this->currentValue]['name'][$f];

        $uzanti = substr($resim, strpos($resim, ".") + 1);

        if (!in_array($turu, $this->turJPEG) && !in_array($uzanti, $this->uzantiJPEG)) {
          if(!empty($this->errors[$this->currentValue]["imageNew"]))
          $this->errors[$this->currentValue]["imageNew"] = array();
          array_push($this->errors[$this->currentValue]["imageNew"], $name .": Lütfen JPEG türünde geçerli resim dosyaları seçiniz.");

        }
      }
      return $this;
    }

    public function mul_isImage_new(){

      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
        $turu = $_FILES[$this->currentValue]['type'][$f];
        $resim = $_FILES[$this->currentValue]['name'][$f];

        $uzanti = substr($resim, strpos($resim, ".") + 1);

        if (!in_array($turu, $this->turler) && !in_array($uzanti, $this->uzantilar)) {

          if(empty($this->errors[$this->currentValue]["imageNew"]))
          $this->errors[$this->currentValue]["imageNew"] = array();
          array_push($this->errors[$this->currentValue]["imageNew"], $name ." Lütfen geçerli resim dosyaları seçiniz.");

        }
      }
      return $this;
    }

    public function countAndCheck($minimum, $maximum){
      $counter = 0;
      // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
      $edit = array_filter($_FILES[$this->currentValue]['name']);

      if(!empty($edit)) {
        foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
          $counter++;
        }

        if($counter < $minimum)
        {
          if(empty($this->errors[$this->currentValue]["counter"]))
            $this->errors[$this->currentValue]["counter"] = array();
          array_push($this->errors[$this->currentValue]["counter"], "Lütfen en az ". $minimum ." adet fotoğraf dosyası seçiniz.");
        }
        elseif($counter > $maximum)
        {
          if(empty($this->errors[$this->currentValue]["counter"]))
            $this->errors[$this->currentValue]["counter"] = array();
          array_push($this->errors[$this->currentValue]["counter"], "En fazla ". $maximum ." adet fotoğraf dosyası seçebilirsiniz.");
        }

      }
      return $this;
    }

    public function mul_checkRes_new($width, $height){

      // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
      $edit = array_filter($_FILES[$this->currentValue]['name']);

      if(!empty($edit)) {
        foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
          $image_info = getimagesize($_FILES[$this->currentValue]["tmp_name"][$f]);
          $image_width = $image_info[0];
          $image_height = $image_info[1];
          if ($image_width > $width || $image_height > $height) {

            if(empty($this->errors[$this->currentValue]["resolutionNew"]))
            $this->errors[$this->currentValue]["resolutionNew"] = array();
            array_push($this->errors[$this->currentValue]["resolutionNew"], $name .": Resim dosyası istenilen (" . $width . " x " . $height . ") çözünürlülükten küçük olmalıdır.");

          }
        }
      }

      return $this;
    }

    public function mul_checkMinRes_new($width, $height){

      // array_filter dizi boş mu kontrolü için gerekli yoksa if'e her türlü giriyor.
      $edit = array_filter($_FILES[$this->currentValue]['name']);

      if(!empty($edit)) {
        foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
          $image_info = getimagesize($_FILES[$this->currentValue]["tmp_name"][$f]);
          $image_width = $image_info[0];
          $image_height = $image_info[1];

          if ($image_width < $width || $image_height < $height) {
            if(empty($this->errors[$this->currentValue]["resolutionNew"]))
            $this->errors[$this->currentValue]["resolutionNew"] = array();
            array_push($this->errors[$this->currentValue]["resolutionNew"], $name .": Resim dosyası istenilen (" . $width . " x " . $height . ") çözünürlülükten büyük olmalıdır.");

          }
        }
      }

      return $this;
    }

    public function mul_checkSize_new($istenenBoyut = 3072){
      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
        $boyut = $_FILES[$this->currentValue]['size'][$f];

        if ($boyut > $istenenBoyut * 1024) {
          if(empty($this->errors[$this->currentValue]["sizeNew"]))
          $this->errors[$this->currentValue]["sizeNew"] = array();
          array_push($this->errors[$this->currentValue]["sizeNew"], $name ." Dosya boyutu " . $istenenBoyut ." KB'dan az olmalıdır.");
        }
      }
      return $this;
    }

    public function mul_checkMinSize_new($istenenBoyut = 1024){
      foreach ($_FILES[$this->currentValue]['name'] as $f => $name) {
        $boyut = $_FILES[$this->currentValue]['size'][$f];

        if ($boyut > $istenenBoyut * 1024) {
          if(empty($this->errors[$this->currentValue]["sizeNew"]))
          $this->errors[$this->currentValue]["sizeNew"] = array();
          array_push($this->errors[$this->currentValue]["sizeNew"], $name ." Dosya boyutu " . $istenenBoyut ." KB'dan fazla olmalıdır.");
        }
      }
      return $this;
    }




    public function checkRes($width, $height){
      if(!empty($this->values[$this->currentValue])) {
        $image_info = getimagesize($_FILES[$this->currentValue]["tmp_name"]);
        $image_width = $image_info[0];
        $image_height = $image_info[1];
        if($image_width > $width || $image_height > $height){
          $this->errors[$this->currentValue]["resolution"] =   "Resim dosyası istenilen (" . $width . " x " . $height . ") çözünürlülükten büyük olmamalıdır." ;
        }
      }


      return $this;
    }

    public function checkMinRes($width, $height){
      if(!empty($this->values[$this->currentValue])) {
        $image_info = getimagesize($_FILES[$this->currentValue]["tmp_name"]);
        $image_width = $image_info[0];
        $image_height = $image_info[1];
        if($image_width < $width || $image_height < $height){
          $this->errors[$this->currentValue]["resolution"] =   "Resim dosyası istenilen (" . $width . " x " . $height . ") çözünürlülükten küçük olmamalıdır." ;
        }
      }


      return $this;
    }


    public function checkSize($istenenBoyut = 3072){
      $boyut = $_FILES[$this->currentValue]['size'];

      if($boyut > $istenenBoyut * 1024){
        $this->errors[$this->currentValue]["size"] =   "Dosya boyutu ". $istenenBoyut ." KB'dan az olmalıdır.";
      }
      return $this;
    }

    public function checkMinSize($istenenBoyut = 1024){
      $boyut = $_FILES[$this->currentValue]['size'];

      if($boyut < $istenenBoyut * 1024){
        $this->errors[$this->currentValue]["size"] =   "Dosya boyutu ". $istenenBoyut ." KB'dan az olmalıdır.";
      }
      return $this;
    }

    public function isMail(){
      if(!filter_var($this->values[$this->currentValue], FILTER_VALIDATE_EMAIL)){
        $this->errors[$this->currentValue]['mail'] = "Lütfen geçerli bir mail adresi giriniz.";

      }
      return $this;
    }

    public function readyIt(){
      $this->values{$this->currentValue} = nl2br(htmlspecialchars($this->values));
    }

    public function length($min,$max){
      if(strlen($this->values[$this->currentValue]) < $min || strlen($this->values[$this->currentValue]) > $max){
        $this->errors[$this->currentValue]["length"] = "Lütfen $min ve $max değerleri arasındaki uzunlukta bir değer giriniz.";
      }
      return $this;
    }

    public function isInt(){
      if(!is_numeric($this->values[$this->currentValue])){
        $this->errors[$this->currentValue]["isInt"] = "Girdiğiniz değer sayı olmalıdır.";
      }
    }

    public function submit(){
      if(empty($this->errors)){
        return true;
      }else{
        return false;
      }
    }

  }
