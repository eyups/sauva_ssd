<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 10.6.2015
 * Time: 11:31
 */

class Captcham {

    public function __construct(){
        Session::init();


        //You can do any necessary settings as you wish here
        //If you reduce the width and height of the captcha here then you have to change it in the css file as well
        $image_width = 200;
        $image_height = 40;
        $characters_on_image = 6; //Number of characters to display on the captcha image
        $font =  dirname(__FILE__) . "/monofont.ttf";

        //The characters that can be used in the CAPTCHA code. Avoid confusing characters (l 1 and i for example)
        $possible_letters = '23456789bcdfghjkmnpqrstvwxyz';
        $random_dots = 10;
        $random_lines = 9;
        $captcha_text_color="c8322d";    //Text color
        $captcha_noice_color = "c8322d"; //Text color

        $code = '';
        $i = 0;
        while ($i < $characters_on_image)
        {
            $code .= substr($possible_letters, mt_rand(0, strlen($possible_letters)-1), 1);
            $i++;
        }
        $font_size = $image_height * 0.79;
        $image = @imagecreate($image_width, $image_height);


        /*Setting the background, text and noise colours here */
        $background_color = imagecolorallocate($image, 255, 255, 255);

        $arr_text_color = $this->rgb_hex($captcha_text_color);
        $text_color = imagecolorallocate($image, $arr_text_color['red'],
            $arr_text_color['green'], $arr_text_color['blue']);
        $arr_noice_color = $this->rgb_hex($captcha_noice_color);
        $image_noise_color = imagecolorallocate($image, $arr_noice_color['red'],
            $arr_noice_color['green'], $arr_noice_color['blue']);

        /*This generates the dots randomly strings in background */
        for( $i=0; $i<$random_dots; $i++ )
        {
            imagefilledellipse($image, mt_rand(0,$image_width),
                mt_rand(0,$image_height), 2, 3, $image_noise_color);
        }

        /*This generates lines randomly strings in background of image */
        for( $i=0; $i<$random_lines; $i++ )
        {
            imageline($image, mt_rand(0,$image_width), mt_rand(0,$image_height),
                mt_rand(0,$image_width), mt_rand(0,$image_height), $image_noise_color);
        }

        Session::set("captcha_code", $code);
        /*This creates a text box and add 6 letters code in it */
        $textbox = imagettfbbox($font_size, 0, $font, $code);
        $x = ($image_width - $textbox[4])/2;
        $y = ($image_height - $textbox[5])/2;
        imagettftext($image, $font_size, 0, $x, $y, $text_color, $font , $code);

        /* Show captcha image in the page html page */
        header('Content-Type: image/jpeg');
        imagejpeg($image);//showing the image
        imagedestroy($image);//destroying the image instance

    }

    public static function rgb_hex ($hexstr)
    {
        $int = hexdec($hexstr);
        return array("red" => 0xFF & ($int >> 0x10),"green" => 0xFF & ($int >> 0x8),"blue" => 0xFF & $int);
    }

}
