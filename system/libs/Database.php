<?php

class Database extends PDO{

    public $link;

    public function __construct($dsn, $user, $password) {

        parent::__construct($dsn, $user, $password);
        $this->query("SET NAMES utf8");
        $this->query("SET CHARACTER SET utf8");
        $this->query("SET COLLATION_CONNECTION='utf8_general_ci'");

        $this->link = @mysql_connect("localhost", $user, $password);
        mysql_select_db(DEBE_NAME, $this->link);

        mb_internal_encoding('UTF-8');
        mysql_query('SET NAMES UTF8');
        mysql_query("SET CHARACTER SET utf8");
        mysql_query("SET COLLATION_CONNECTION = 'utf8_general_ci'");


    }

    function mysql_fetch_all ($result, $result_type = MYSQL_BOTH)
    {
        if (!is_resource($result) || get_resource_type($result) != 'mysql result')
        {
            trigger_error(__FUNCTION__ . '(): supplied argument is not a valid MySQL result resource', E_USER_WARNING);
            return false;
        }
        if (!in_array($result_type, array(MYSQL_ASSOC, MYSQL_BOTH, MYSQL_NUM), true))
        {
            trigger_error(__FUNCTION__ . '(): result type should be MYSQL_NUM, MYSQL_ASSOC, or MYSQL_BOTH', E_USER_WARNING);
            return false;
        }
        $rows = array();
        while ($row = mysql_fetch_array($result, $result_type))
        {
            $rows[] = $row;
        }
        return $rows;
    }

    public static function interpolateQuery($query, $params) {
        $keys = array();

        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }

            if(!is_numeric($value)){
                $params[$key] = "'".$value."'";
            }
        }

        $query = preg_replace($keys, $params, $query, 1, $count);

        return $query;
    }

    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC){
        /*$sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }

        $sth->execute();

        return $sth->fetchAll($fetchMode);*/

        $sql =  self::interpolateQuery($sql, $array);

        $result = mysql_query($sql, $this->link);

        return $this->mysql_fetch_all($result);

    }

    public function affectedRows($sql, $array = array()){
        /*$sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }
        $sth->execute(); */
        $sql =  self::interpolateQuery($sql, $array);

        $result = mysql_query($sql, $this->link);

        $count = mysql_num_rows($result);
        //echo $sql;
        return $count;

        //return $sth->rowCount();
    }

    /**
     * @param $tableName
     * @param $data
     * @return bool
     */
    public function insert($tableName, $data){

        $fieldKeys = implode(",", array_keys($data));
        $fieldValues = ":" . implode(", :", array_keys($data));

        $sql = "INSERT INTO $tableName($fieldKeys) VALUES ($fieldValues)";
        $sth = $this->prepare($sql);

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        return array($sth->execute(), $sth->errorInfo());

    }

    public function update($tableName, $data, $where){
        $updateKeys = null;

        foreach ($data as $key => $value){
            $updateKeys .= "$key=:$key,";

        }
        $updateKeys = rtrim($updateKeys, ",");
        $sql = "UPDATE $tableName SET $updateKeys WHERE $where";

        $sth = $this->prepare($sql);

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        return array($sth->execute(), $sth->errorInfo());
    }

    public function delete($tableName, $where, $limit = 1){
        $sql = "DELETE FROM $tableName WHERE $where LIMIT $limit";

        $sth = $this->prepare($sql);

        return array($sth->execute(), $sth->errorInfo());

    }


}
/*

<?php

class Database extends PDO{

    public function __construct($dsn, $user, $password) {
        parent::__construct($dsn, $user, $password);
        $this->query("SET NAMES utf8");
        $this->query("SET CHARACTER SET utf8");
        $this->query("SET COLLATION_CONNECTION='utf8_general_ci'");



    }

    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC){
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }
        $sth->execute();

        return $sth->fetchAll($fetchMode);
    }

    public function affectedRows($sql, $array = array()){
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }
        $sth->execute();
        return $sth->rowCount();
    }


    public function insert($tableName, $data){

        $fieldKeys = implode(",", array_keys($data));
        $fieldValues = ":" . implode(", :", array_keys($data));

        $sql = "INSERT INTO $tableName($fieldKeys) VALUES ($fieldValues)";
        $sth = $this->prepare($sql);

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        return array($sth->execute(), $sth->errorInfo());

    }

    public function update($tableName, $data, $where){
        $updateKeys = null;

        foreach ($data as $key => $value){
            $updateKeys .= "$key=:$key,";

        }
        $updateKeys = rtrim($updateKeys, ",");
        $sql = "UPDATE $tableName SET $updateKeys WHERE $where";

        $sth = $this->prepare($sql);

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        return array($sth->execute(), $sth->errorInfo());
    }

    public function delete($tableName, $where, $limit = 1){
        $sql = "DELETE FROM $tableName WHERE $where LIMIT $limit";

        $sth = $this->prepare($sql);

        return array($sth->execute(), $sth->errorInfo());

    }


}
*/