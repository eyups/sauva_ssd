<?php

class Model{
    
    protected $db = array();
    
    public function __construct() {
        $dsn = 'mysql:dbname=' . DEBE_NAME . ';host=' . DEBE_HOST;
        $user = DEBE_USER;
        $password = DEBE_PASS;

        try {
            $this->db = new Database($dsn, $user, $password);
        }catch (PDOException $hata){
            $load   =   new Load();
            $load->view("Site/teknikSorun");
            exit();
        }
    }

    public function icerikListele($table, $order = null, $ascOrdesc = "ASC"){
        if($order == null){
            $sql = "SELECT * FROM $table";
        }else{
            $sql = "SELECT * FROM $table ORDER BY $order $ascOrdesc";
        }

        return $this->db->select($sql);
    }

    public function icerikEkle($table, $data){
        return $this->db->insert($table, $data);
    }

    public function icerikGuncelle($table, $data, $where){
        return $this->db->update($table, $data, $where);
    }



}
