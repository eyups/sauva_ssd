<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 8.6.2015
 * Time: 23:49
 */

class Session {

    public static function init(){
        session_start();
    }

    public static function set($key, $val){
        $_SESSION[$key] = $val;
    }

    public static function get($key){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }else{
            return false;
        }

    }

    public static function destroy(){
        session_destroy();
    }

} 