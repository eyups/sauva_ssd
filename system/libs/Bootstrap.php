<?php
/**
 * Created by PhpStorm.
 * User: EyüpSabri
 * Date: 8.6.2015
 * Time: 23:44
 *  Bootstrap loads our controllers and their methods
 */

class Bootstrap {

    public $_url;
    public $_controllerName = "Index";
    public $_methodName = "Index";
    public $_controller;

    public $_controllerPath = "app/controllers/";

    public function __construct(){
        // Bootstrap Bölümü
        $this->getUrl();
        $this->loadController();

        $this->callMethod();
    }

    public function getUrl(){
        $this->_url = isset($_GET["url"]) ? $_GET["url"] : null;
        if( $this->_url!= null){
            $this->_url = rtrim( $this->_url, "/");
            $this->_url = explode("/",  $this->_url);
        }else{
            unset( $this->_url);
        }
    }

    public function loadController(){

        //URL controller kısmı set edilmemişse anasayfaya yönnlendir.
        if(!isset($this->_url[0])){
            include  $this->_controllerPath . $this->_controllerName . ".php";
            $this->_controller = new $this->_controllerName();
        }
        // Set edilmişse ilgili controller dosyasını kontrol et, classını kontrol et
        else{
            $this->_controllerName = $this->_url[0];
            $fileName = $this->_controllerPath . $this->_controllerName . ".php";
            // Dosya var mı kontrol
            if(file_exists($fileName)){
                include  $fileName;
                // Class var mı kontrol
                if(class_exists($this->_controllerName)) {
                    $this->_controller = new $this->_controllerName();

                }else{
                    $load   =   new Load();
                    header("HTTP/1.1 404 Not Found");
                    $load->view("Site/404");
                }
            }else{
            }
        }


    }

    public function callMethod(){

        if (isset($this->_url[2])) {
            $this->_methodName = $this->_url[1];

            if(method_exists($this->_controller, $this->_methodName)) {
                $this->_controller->{$this->_methodName}($this->_url[2]);
            }else {
                //echo " Controller içinde method bulunamadı.";
                $load   =   new Load();
                header("HTTP/1.1 404 Not Found");
                $load->view("Site/404");

            }

        }
        else {

            if (isset($this->_url[1])) {
                $this->_methodName = $this->_url[1];
                if(method_exists($this->_controller, $this->_methodName)) {
                    $this->_controller->{$this->_methodName}();
                }else if($this->_controllerName == "Sayfa"){
                    $this->_controller->{"sayfaGoster"}($this->_methodName);
                }else if($this->_controllerName == "Duyuru"){
                    $this->_controller->{"index"}($this->_methodName);
                }
                else if($this->_controllerName == "Brosurler"){
                    $this->_controller->{"index"}($this->_methodName);
                }
                else {
                   // echo "Controller içinde method bulunamadızzz.";
                    header("HTTP/1.1 404 Not Found");
                    $load   =   new Load();
                    $load->view("Site/404");
                }
            } else {
                if(method_exists($this->_controller, $this->_methodName)) {
                    $this->_controller->{$this->_methodName}();
                }else {
                    //echo "Controller içinde index bulunamadı.";
                    header("HTTP/1.1 404 Not Found");
                    $load   =   new Load();
                    $load->view("Site/404");
                }
            }
        }


    }

}