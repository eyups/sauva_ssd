<?php

class Load{
    public function __construct() {
        
    }
    
    public function view($fileName, $data = false){
        if($data == true){
            extract($data);
        }
        include "app/views/" . $fileName . "_view.php";
    }
    
    public function model($fileName){
        $modelName = $fileName. "_Model";
        include "app/models/" . $modelName . ".php";
        return new $modelName();
    }

    public function otherClasses($fileName, $param1 = null, $param2 = null){
        include "app/otherClasses/" . $fileName . ".php";

        if(!empty($param1) && !empty($param2)){
            return new $fileName($param1, $param2);
        }else{
            return new $fileName();
        }

    }

    public function PHPMailer(){
        include "app/otherClasses/PHPMailerAutoload.php";
        return new PHPMailer();
    }

    public function PHPExcel(){
        include "app/otherClasses/PHPExcel.php";
        return new PHPExcel();
    }



}
